import argparse
import logging
import os
import numpy as np
from gwdatafind import find_urls

from pointy import pipeline


parser = argparse.ArgumentParser()
parser.add_argument('time', type=float, help='event t0')
args = parser.parse_args()

# ---- settings ----
time = args.time

buffer = 10  # seconds around main time to query datafind
# -- kw settings ---
stride = 16
basename = "-KW_TRIGGERS"
segname = "-KW_SEGMENTS"
significance = 15.0
threshold = 3.0
decimateFactor = -1
trailing_frametype = '_R'
channel = "CAL-DELTAL_EXTERNAL_DQ"
f_low = 16.0
f_high = 2048.0

kw_window = 0.02

# ---- pointy settings -----
# set settings for pointy pipeline
# see pointy configs in ~/geoffrey.mo/hwinj/sep2019/H1_run1/run.sh for defs
pp_hoft_surrogate = "_CAL-DELTAL_EXTERNAL_DQ_32_2048"

pp_srate = 128

# computational optimizations
pp_window = 1
pp_active_signif = 0

# rate estimation parameters
pp_thresholds = [15, 20, 25, 30, 35, 50, 75, 100, 500, 1000]

pp_left_rate_estimation_window = 1800
pp_right_rate_estimation_window = 3600

# multi-channel analyses parameters
pp_target_signif = 35

# plotting and post-procesing parameters
pp_zoom_window = 10 * pp_window
pp_cluster_window = pp_window
pp_fap_thresholds = [0.1, 0.05, 0.01, 0.005, 0.001]


# ---- helper functions ----


def find_frames_to_txt(ifo, time, buffer, wd):
    # Find frames and get them into the form KW wants
    frames = find_urls(ifo[0], ifo + trailing_frametype,
                       time - buffer, time + buffer)
    frames_kw_format = []
    for frame in frames:
        frames_kw_format.append(frame.split('localhost')[-1])
    pathname = os.path.join(wd, "{}_{}_frame.txt".format(
        ifo, int(time)))
    with open(pathname, "w+") as f:
        f.write('\n'.join(frames_kw_format))
    return pathname


def build_kw_config(wd, ifo, stride, basename, segname, significance,
                    threshold, decimateFactor, channel, f_low, f_high):
    pathname = os.path.join(wd, "{}-KW_STRAIN.cfg".format(ifo))
    with open(pathname, "w+") as f:
        f.write("stride {}\n".format(stride))
        f.write("basename {}{}\n".format(ifo[0], basename))
        f.write("segname {}{}\n".format(ifo[0], segname))
        f.write("significance {}\n".format(significance))
        f.write("threshold {}\n".format(threshold))
        f.write("decimateFactor {}\n".format(decimateFactor))
        f.write("channel {}:{} {} {}\n".format(
            ifo, channel, f_low, f_high))
    return pathname


def run_kw(wd, ifo, time):
    os.system("cd {} && kleineWelleM {} -inlist {}".format(
        wd,
        os.path.join(wd, "{}-KW_STRAIN.cfg".format(ifo)),
        os.path.join(wd, "{}_{}_frame.txt".format(ifo, int(time)))))
    header = ("\# start_time stop_time time frequency unnormalized_energy "
              "normalized_energy chisqdof significance channel")
    alltrgs = "{}/{}-KW_TRIGGERS-{}/{}-KW_TRIGGERS-ALL.trg".format(
        wd, ifo[0], str(time)[:5], ifo[0])
    os.system("echo {} > {}".format(header, alltrgs))
    os.system(('find {}/{}-KW_TRIGGERS-{} -name "{}*.trg" | xargs -n 1 '
               'tail -n +2 >> {}').format(
                   wd, ifo[0], str(time)[:5], ifo[0], alltrgs))
    return alltrgs


def match_kw_time(dialed_time, kwfile, window):
    trgs = []
    with open(kwfile, 'r') as f:
        rows = f.readlines()
        for row in rows:
            if row[0] == '#':
                continue
            trg = float(row.split()[2])
            dt = abs(dialed_time - trg)
            trgs.append((trg, dt))
    trgs.sort(key=lambda x: x[1])
    for trg in trgs:
        if trg[1] < window:
            return np.float64(trg[0])
        else:
            return None


def run_pointy(wd, time, ifo, kw_time_file):
    pp_output_dir = wd
    pp_tag = '{}_{}'.format(ifo, time)
    pp_stdout = os.path.join(wd, 'pp_stdout.txt')
    pp_stderr = os.path.join(wd, 'pp_stderr.txt')

    frametype = '{ifo}{ftype}'.format(ifo=ifo, ftype=trailing_frametype)

    target_channel = '{ifo}{hoft_surr}'.format(
        ifo=ifo, hoft_surr=pp_hoft_surrogate)
    gps_group = [('bbh', kw_time_file)]

    gps_groups = dict(
        (name, np.loadtxt(path, ndmin=1)) for name, path in gps_group)

    gps = np.concatenate(tuple(gps_groups.values()))
    start = np.floor(min(gps) - pp_window)
    stop = np.ceil(max(gps) + pp_window)


    timeseries, _, targets = pipeline.analyze(
        frametype,
        start,
        stop,
        target_channel, ### should be KW naming convention
        target_signif=pp_target_signif,
        srate=pp_srate,
        active_signif=pp_active_signif,
        thresholds=pp_thresholds,
        left_coinc_window=pp_window,
        right_coinc_window=pp_window,
        left_rate_window=pp_left_rate_estimation_window,
        right_rate_window=pp_right_rate_estimation_window,
        output_dir=pp_output_dir,
        tag=pp_tag,
        verbose=True,
        Verbose=True,
        skip_multi_channel=True,
        statistics=False,
    )

    return timeseries, targets


def get_tag(filepath, time, ifo):
    return os.path.split(filepath)[-1].split('_{}_'.format(ifo))[0] \
            + '_' + str(int(time))


# ----------------------

for ifo in ['H1', 'L1']:
    try:
        os.mkdir(str(int(time)))
    except OSError:
        pass
    wd = os.path.join(os.getcwd(), str(int(time)))

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s : %(levelname)s : %(name)s : %(message)s')

    logging.info('Now processing: {}, {}'.format(ifo, time))

    logging.info('Finding frames for KW')
    kw_frames = find_frames_to_txt(ifo, time, buffer, wd)
    logging.info('Found frames for KW')
    kw_config = build_kw_config(
        wd, ifo, stride, basename, segname, significance,
        threshold, decimateFactor, channel, f_low, f_high)

    # Make sure these files were actually created
    for file in [kw_frames, kw_config]:
        with open(file) as f:
            try:
                assert len(f.readlines()) > 0
            except AssertionError:
                log.exception("KW config not created")

    # Run KW
    logging.info('Running KW')
    kw_trgfile = run_kw(wd, ifo, time)
    # Match dialed time to time picked up by KW
    logging.info('Matching KW picked-up time to dialed time')
    kw_time = match_kw_time(time, kw_trgfile, kw_window)
    # Record this time in the working directory for use by Pointy
    kw_time_file = os.path.join(wd, '{}-time.txt'.format(ifo))
    with open(kw_time_file, 'w+') as f:
        f.write(str(kw_time) + '\n')
    print(kw_time_file)
    logging.info('KW picked-up time saved to {}'.format(kw_time_file))
    # Run Pointy
    logging.info('Running pointy.pipeline.analyze')
    timeseries, targets = run_pointy(wd, time, ifo, kw_time_file)


    ts_dir = os.path.join(wd, "timeseries_{}_{}".format(
        ifo, round(time, 2)))

    orig_ts_list = os.listdir(ts_dir)
    ts_list = [os.path.join(ts_dir, ts) for ts in orig_ts_list]
    stacked_outdir = os.path.join(wd,"{}_stacked".format(ifo))

    for ts in ts_list:
        pair = '{},{}'.format(ts, kw_time)
        cmd = 'logpvalue2stacked {} -t {tag} -o {outdir} -v'.format(
            pair,
            tag=get_tag(ts, time),
            outdir=stacked_outdir)
        logging.info('Computing stacked p-value for {}'.format(ts))
        os.system(cmd)
        logging.info('Stacked p-value for {} computed'.format(ts))

    orig_stacked_list = os.listdir(stacked_outdir)
    stacked_list = [os.path.join(stacked_outdir, stacked)
                    for stacked in orig_stacked_list]

    chanlist_outdir = os.path.join(wd, "{}_chanlists".format(ifo))

    logging.info('Stringing together channel and logpvalue.npy pairs')

    chan_path_pairs = ["{},{}".format(
        os.path.split(
            path)[-1].split('stacked_')[-1].split(str(int(time)))[0], path)
        for path in stacked_list]

    # break pairs into manageable sizes (lists of len = 100 each)
    n = 100
    logging.info('Breaking apart pairs into manageable sizes')
    chan_npy_pairs_broken = [chan_path_pairs[i*n : (i+1)*n]
                             for i in range(
                                 (len(chan_path_pairs) + n - 1) // n)]
    for i, chunk in enumerate(chan_npy_pairs_broken):
        logging.info('Calculating channels for chunk {} of {}'.format(
            i, len(chan_npy_pairs_broken)))

        # make temporary directory
        tempdirpath = os.path.join(chanlist_outdir, "temp{}".format(i))
        try:
            os.makedirs(tempdirpath)
        except OSError:
            pass

        # string together command arguments
        chan_npy_pairs_str = " ".join(chunk)

    logging.info("Calling logpvalue2chans for this chunk")
    command = ("logpvalue2chans -v"
               " --output-dir {} {}").format(
                    tempdirpath, chan_npy_pairs_str)
    # actually run the thing
    os.system(command)
    logging.info("Channels categorized")

    # cat results and clean up dirs
    logging.info("Combining results from different chunks")
    for resulttype in ["ok", "danger", "warning"]:
        os.system(("cat {chanlist_outdir}/temp*/logpvalue2chans-{restype}* > "
                   "{chanlist_outdir}/logpvalue2chans-{restype}.txt").format(
                       chanlist_outdir=chanlist_outdir, restype=resulttype))
    os.system("rm -r {}/temp*".format(chanlist_outdir))
    logging.info("Complete!")

logging.info("The whole thing is complete. Enjoy!")
