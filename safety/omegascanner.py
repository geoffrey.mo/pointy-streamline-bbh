import argparse
from gwpy.timeseries import TimeSeries
from gwdatafind import find_urls
import matplotlib.pyplot as plt
import os

plt.rcParams.update({"text.usetex": False})

def main(ifo, start, duration, outdir, channelnames, inj_time_txt=None):
    end = start + duration
    filenames = find_urls(ifo[0], '{}_R'.format(ifo), start, end)

    if inj_time_txt:
        with open(inj_time_txt, 'r') as f:
            inj_times = f.read().splitlines()
            inj_times = [float(time) for time in inj_times]

    for channelname in channelnames:
        print("making omegascans for {}".format(channelname))

        try:
            ts = TimeSeries.read(filenames, channelname, start=start, end=end)
            qspecgram = ts.q_transform(frange=(20,4000),
                                        outseg=(start, end))
            plt = qspecgram.plot(figsize=[10, 6])
            ax = plt.gca()
            ax.set_xscale('seconds')
            ax.set_yscale('log')
            ax.set_ylim(20, 4000)
            ax.set_ylabel('Frequency [Hz]')
            ax.set_title(channelname)
            ax.grid(True, axis='y', which='both')
            ax.colorbar(cmap='viridis', label='Normalized energy', clim=(0, 30))
            if inj_time_txt:
                for time in inj_times:
                    ax.axvline(time, color='white', alpha=0.5, linestyle='--', lw=0.7)
            ax.set_xlim(start, end)
            plt.savefig(os.path.join(outdir, channelname + '.png'), dpi=225)


        except FloatingPointError:
            with open(os.path.join(outdir, channelname + '_failed.txt'), "w+") as f:
                f.write("this failed with a floating point error.\n")

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('ifo', type=str)
    parser.add_argument('start', type=float)
    parser.add_argument('duration', type=float)
    parser.add_argument('outdir', type=str)
    parser.add_argument('--inj-time-txt', type=str, default=None)
    parser.add_argument('channelnames', type=str, nargs='+')
    args = parser.parse_args()

    main(args.ifo, args.start, args.duration, args.outdir, args.channelnames,
         inj_time_txt=args.inj_time_txt)
