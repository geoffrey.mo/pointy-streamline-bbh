import argparse
import os
import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument('ifo', type=str)
parser.add_argument('chan_dir', type=str)
parser.add_argument('output_name', type=str)
args = parser.parse_args()

danger = np.loadtxt(os.path.join(args.chan_dir, 'logpvalue2chans-danger.txt'),
                                 dtype='|S100,float')
warning = np.loadtxt(os.path.join(args.chan_dir, 'logpvalue2chans-warning.txt'),
                                 dtype='|S100,float')
ok = np.loadtxt(os.path.join(args.chan_dir, 'logpvalue2chans-ok.txt'),
                            dtype='|S100,float')
all_array = np.concatenate([danger, warning, ok])

pvals = [v for k, v in all_array]

plt.hist(pvals, bins=50, histtype="step")
plt.yscale('log')
plt.ylabel('Number of channels')
plt.xlabel('Mean p-value')
plt.title(args.chan_dir + " p-value distribution")
plt.savefig(args.output_name, dpi=250)
