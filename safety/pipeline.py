import argparse
import os
import sys

import safety
import utils

parser = argparse.ArgumentParser()
parser.add_argument('ifo', type=str, help='ifo to analyze ("H1" or "L1")')
parser.add_argument('wd', type=str, help='working directory for this run')
parser.add_argument('tagname', type=str, help='tagname for this run')
parser.add_argument('t_start', help='start gpstime of injection waveform')

args = parser.parse_args()
ifo = args.ifo
wd = os.path.abspath(args.wd)
tagname = args.tagname
t_start = float(args.t_start)

try:
    os.makedirs(wd)
except OSError:  # directory already exists
    pass
sys.stdout = open(os.path.join(wd, "{}_{}.out".format(ifo, tagname)), "w+")
sys.stderr = open(os.path.join(wd, "{}_{}.err".format(ifo, tagname)), "w+")

kw_all = utils.inj_t1900555_to_txt(ifo, t_start, tagname, outputdir=wd)
kwtrgfile = os.path.join(
    wd, "{}-KW_TRIGGERS-{}/{}-KW_TRIGGERS-ALL.trg".format(
        ifo[0], str(t_start)[:5], ifo[0]))
utils.plot_inj_vs_kw(ifo, t_start, tagname, kwtrgfile, outputdir=wd)

gps_groups = [('ALL', kw_all)]

rdschan_path = safety.make_big_rds_channellist(ifo, wd, tagname, gps_groups)
subsys_list = safety.split_into_subsys(ifo, wd, rdschan_path)

async_args_tuple = tuple([
    tuple([ifo, wd, tagname, subsys, gps_groups]) for subsys in subsys_list])
utils.do_asynchronously(
    safety.make_pval_timeseries_to_stacked_for_subsys, async_args_tuple)

safety.stacked_to_chanlists(ifo, wd, tagname)
