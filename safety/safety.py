import argparse
import glob
import logging
import multiprocessing as mp
import os
import re

import numpy as np
from gwdatafind import find_urls
from pointy import pipeline, html
from pointy import utils as putils
import matplotlib.pyplot as plt

import utils

# ---- pointy settings -----
# see pointy configs in ~/geoffrey.mo/hwinj/sep2019/H1_run1/run.sh for defs
DEFAULT_TRAILING_FRAMETYPE= "_R"
DEFAULT_PP_HOFT_SURROGATE = "CAL-DELTAL_EXTERNAL_DQ_32_2048"
DEFAULT_PP_HOFT_SURROGATE_SRATE = 16384

DEFAULT_PP_SRATE = 128

# computational optimizations
DEFAULT_PP_WINDOW = 1
DEFAULT_PP_ACTIVE_SIGNIF = 0

# rate estimation parameters
DEFAULT_PP_THRESHOLDS = [15, 20, 25, 30, 35, 50, 75, 100, 500, 1000]

DEFAULT_PP_LEFT_RATE_ESTIMATION_WINDOW = 1800
DEFAULT_PP_RIGHT_RATE_ESTIMATION_WINDOW = 3600

# multi-channel analyses parameters
DEFAULT_PP_TARGET_SIGNIF = 35
DEFAULT_FRAC_DUR = 0.01

# plotting and post-procesing parameters
DEFAULT_PP_ZOOM_WINDOW = 10 * DEFAULT_PP_WINDOW
DEFAULT_PP_CLUSTER_WINDOW = DEFAULT_PP_WINDOW
DEFAULT_PP_FAP_THRESHOLDS = [0.1, 0.05, 0.01, 0.005, 0.001]

# pvalue thresholds
DEFAULT_PVALUE_DANGER = np.e**(-2)
DEFAULT_PVALUE_WARNING = np.e**(-1.5)

def make_big_rds_channellist(
    ifo, wd, tagname, gps_groups,
    trailing_frametype=DEFAULT_TRAILING_FRAMETYPE,
    pp_target_signif=DEFAULT_PP_TARGET_SIGNIF,
    pp_hoft_surrogate=DEFAULT_PP_HOFT_SURROGATE,
    pp_srate=DEFAULT_PP_SRATE, pp_active_signif=DEFAULT_PP_ACTIVE_SIGNIF,
    pp_thresholds=DEFAULT_PP_THRESHOLDS, pp_window=DEFAULT_PP_WINDOW,
    pp_left_rate_estimation_window=DEFAULT_PP_LEFT_RATE_ESTIMATION_WINDOW,
    pp_right_rate_estimation_window=DEFAULT_PP_RIGHT_RATE_ESTIMATION_WINDOW,
    frac_dur=DEFAULT_FRAC_DUR
                            ):
    """Generate the RDS channel list that says what's active

    Parameters
    ---------
    ifo: str
        "H1" or "L1"
    wd: str
        Path to working directory
    tagname: str
        Tagname for this pointy run
    gps_groups: list of tuples
        List of tuples (description, path to gpstimes.txt).
        For example, [('SNR100_100Hz', '/path/to/snr100_100hz.txt'),
                      ('ALL', '/path/to/all.txt')]

    Returns
    -------
    str: path to rds_chanlist file
    """
    frametype = '{ifo}{ftype}'.format(ifo=ifo, ftype=trailing_frametype)
    target_channel = '{ifo}_{hoft_surr}'.format(
        ifo=ifo, hoft_surr=pp_hoft_surrogate)

    gps_groups_dict = dict(
        (name, np.loadtxt(path, ndmin=1)) for name, path in gps_groups)

    gps = np.concatenate(tuple(gps_groups_dict.values()))
    start = np.floor(min(gps) - pp_window)
    stop = np.ceil(max(gps) + pp_window)

    pipeline.analyze(
        frametype,
        start,
        stop,
        target_channel, ### should be KW naming convention
        target_signif=pp_target_signif,
        srate=pp_srate,
        active_signif=pp_active_signif,
        thresholds=pp_thresholds,
        left_coinc_window=pp_window,
        right_coinc_window=pp_window,
        left_rate_window=pp_left_rate_estimation_window,
        right_rate_window=pp_right_rate_estimation_window,
        frac_dur=frac_dur,
        output_dir=wd,
        tag=tagname,
        verbose=True,
        Verbose=True,
        generate_timeseries=False,
        skip_multi_channel=True,
        skip_rate_trigger_generation=True,
        statistics=False,
    )

    rds_chanlists = glob.glob(
        os.path.join(wd, "*{}*rds_chanlist*txt".format(tagname)))
    rdschan_path = os.path.abspath(rds_chanlists[0])
    return rdschan_path


def make_pval_timeseries(
    ifo, wd, tagname, gps_groups, chanlist,
    trailing_frametype=DEFAULT_TRAILING_FRAMETYPE,
    pp_target_signif=DEFAULT_PP_TARGET_SIGNIF,
    pp_hoft_surrogate=DEFAULT_PP_HOFT_SURROGATE,
    pp_srate=DEFAULT_PP_SRATE, pp_active_signif=DEFAULT_PP_ACTIVE_SIGNIF,
    pp_thresholds=DEFAULT_PP_THRESHOLDS, pp_window=DEFAULT_PP_WINDOW,
    pp_left_rate_estimation_window=DEFAULT_PP_LEFT_RATE_ESTIMATION_WINDOW,
    pp_right_rate_estimation_window=DEFAULT_PP_RIGHT_RATE_ESTIMATION_WINDOW,
    frac_dur=DEFAULT_FRAC_DUR
                            ):
    """Generate the pvalue timeseries with pointy.analyze. In the greater
    pipeline, this needs to be run for each subsystem, since running
    it all at once will break the script (too many bash lines, things like
    that).

    Parameters
    ---------
    ifo: str
        "H1" or "L1"
    wd: str
        Path to working directory. If after subsys split, this should be the
        subsys directory.
    tagname: str
        Tagname for this pointy run
    gps_groups: list of tuples
        List of tuples (description, path to gpstimes.txt).
        For example, [('SNR100_100Hz', '/path/to/snr100_100hz.txt'),
                      ('ALL', '/path/to/all.txt')]
    chanlist: str
        Path to ascii file with "channel_name sample_rate" on each line

    Returns
    -------
    None, but the files should have been written out
    """
    frametype = '{ifo}{ftype}'.format(ifo=ifo, ftype=trailing_frametype)
    target_channel = '{ifo}_{hoft_surr}'.format(
        ifo=ifo, hoft_surr=pp_hoft_surrogate)

    gps_groups_dict = dict(
        (name, np.loadtxt(path, ndmin=1)) for name, path in gps_groups)

    gps = np.concatenate(tuple(gps_groups_dict.values()))
    start = np.floor(min(gps) - pp_window)
    stop = np.ceil(max(gps) + pp_window)

    pipeline.analyze(
        frametype,
        start,
        stop,
        target_channel, ### should be KW naming convention
        target_signif=pp_target_signif,
        srate=pp_srate,
        active_signif=pp_active_signif,
        thresholds=pp_thresholds,
        left_coinc_window=pp_window,
        right_coinc_window=pp_window,
        left_rate_window=pp_left_rate_estimation_window,
        right_rate_window=pp_right_rate_estimation_window,
        frac_dur=frac_dur,
        output_dir=wd,
        tag=tagname,
        chanlist_path=chanlist,
        verbose=True,
        Verbose=True,
        generate_timeseries=True,
        skip_multi_channel=True,
        statistics=False,
    )

    return None


def split_into_subsys(ifo, wd, chanlist,
                      max_N_chans=50,
                      hoft_surrogate=DEFAULT_PP_HOFT_SURROGATE,
                      hoft_surrogate_srate=DEFAULT_PP_HOFT_SURROGATE_SRATE,
                     ):
    """Python wrapper around a bunch of bash which splits the directories
    into their subsystems

    Parameters
    ----------
    ifo: str
        "H1" or "L1"
    wd: str
        Path to working directory
    chanlist: str
        Path to rds_chanlist.txt containing list of active channels
    max_N_chans: int, optional
        Maximum number of channels in a subsystem directory.
        If the number of channels for a subsystem is greater, than this,
        we will split them into multiple directories.
    hoft_surrogate: str, optional
        Name of h(t) surrogate, without ifo prefix,
        eg. "CAL-DELTAL_EXTERNAL_DQ_32_2048"
    hoft_surrogate_srate: int, optional
        h(t) surrogate sampling rate, eg. 16384

    Returns
    -------
    list: subsys_list is returned
    """
    # Get the subsystems that exist in the channel list
    with open(chanlist) as cl:
        channellist = [line for line in cl]
    channellist.sort()
    subsys_list = [line.split(':')[-1].split('-')[0] for line in channellist]
    subsys_list = list(set(subsys_list))
    subsys_list.sort()

    # count channels in each subsystem
    num_chans = {subsys: 0 for subsys in subsys_list}
    for line in channellist:
        num_chans[line[3:6]] += 1

    # Make subsys directories and populate them with channel lists
    subsys_full_list = []
    for subsys_main in subsys_list:
        # get all channels from this subsystem
        subsys_chans = [line for line in channellist
                        if '{}:{}'.format(ifo, subsys_main) in line]
        subsys_chans_split = [
            subsys_chans[i:i + max_N_chans]
            for i in range(0, len(subsys_chans), max_N_chans)
        ]
        dirs_necessary = num_chans[subsys_main] // max_N_chans + 1
        # go through each necessary directory
        for i in range(dirs_necessary):
            subsys_dir_chans = []
            subsys = "{}_{}".format(subsys_main, i)
            subsys_full_list.append(subsys)
            dirpath = os.path.join(wd, '{}_{}'.format(ifo, subsys))
            try:
                os.mkdir(dirpath)
            except OSError:  # directory already exists
                pass

            # get rid of the KW frequency in the hoft surrogate
            hoft_surrogate_without_freqs = '_'.join(hoft_surrogate.split('_')[:-2])
            # add the target channel to the chanlist
            subsys_dir_chans.append(
                '{}:{} {}\n'.format(
                    ifo, hoft_surrogate_without_freqs, hoft_surrogate_srate)
            )
            # add the active channels for this subsys to the chanlist
            subsys_dir_chans += subsys_chans_split[i]

            # write these channels to a file in the new directory
            with open(os.path.join(dirpath, 'channels.txt'), 'w+') as c:
                c.write(''.join(subsys_dir_chans))
    return subsys_full_list


def timeseries_to_stacked(ifo, wd, tagname, gps_groups, normalize=True):
    """Go from a pvalue timeseries to a stacked pvalue, injection times.
    This needs to be run for each subsystem, since running
    it all at once will break the script (too many bash lines, things like
    that).

    Assumes that timeseries_{TAGNAME} exists and is populated in the working
    directory.

    # FIXME: need to check if this is doing something wrong with how
    logpvalue2stacked calls pointy.lognaivebayes() for actually stacking

    Parameters
    ---------
    ifo: str
        "H1" or "L1"
    wd: str
        Path to working directory. If after subsys split, this should be the
        subsys directory.
    tagname: str
        Tagname for this pointy run
    gps_groups: list of tuples
        List of tuples (description, path to gpstimes.txt).
        For example, [('SNR100_100Hz', '/path/to/snr100_100hz.txt'),
                      ('ALL', '/path/to/all.txt')]
    normalize: bool, optional
        Take the average of pvalues instead of just the sum

    Returns
    -------
    None, but the files should have been written out
    """
    # Get the timeseries that exist inside the wd
    timeseries_npys = glob.glob(
        os.path.join(wd, "timeseries_{}/*npy".format(tagname)))
    # If we didn't find any timeseries npy files, raise an error
    if len(timeseries_npys) == 0:
        raise RuntimeError(("The timeseries_{} directory does not exist ")
                           ("inside the given working directory.").format(
                               tagname))
    timeseries_npys = [os.path.abspath(path) for path in timeseries_npys]
    # Get the gpstimes we are stacking
    gps_groups_dict = dict(
        (name, np.loadtxt(path, ndmin=1)) for name, path in gps_groups)
    gps = np.concatenate(tuple(gps_groups_dict.values()))
    # Set up output directory path (will be created by logpvalue2stacked)
    outdir = os.path.join(wd, "stacked_{}".format(tagname))
    normalized = "--normalize " if normalize else ""

    for timeseries_npy in timeseries_npys:
        args = " ".join(["{},{}".format(timeseries_npy, str(gpstime))
                         for gpstime in gps])
        channelname = os.path.basename(timeseries_npy).split('_'+tagname)[0]
        tag = '{}_{}'.format(channelname, tagname)
        command = "logpvalue2stacked -v -o {} {}-t {} {}".format(
            outdir, normalized, tag, args)
        os.system(command)


def make_pval_timeseries_to_stacked_for_subsys(
    ifo, wd, tagname, subsys, gps_groups,
    trailing_frametype=DEFAULT_TRAILING_FRAMETYPE,
    pp_target_signif=DEFAULT_PP_TARGET_SIGNIF,
    pp_hoft_surrogate=DEFAULT_PP_HOFT_SURROGATE,
    pp_srate=DEFAULT_PP_SRATE, pp_active_signif=DEFAULT_PP_ACTIVE_SIGNIF,
    pp_thresholds=DEFAULT_PP_THRESHOLDS, pp_window=DEFAULT_PP_WINDOW,
    pp_left_rate_estimation_window=DEFAULT_PP_LEFT_RATE_ESTIMATION_WINDOW,
    pp_right_rate_estimation_window=DEFAULT_PP_RIGHT_RATE_ESTIMATION_WINDOW,
    frac_dur=DEFAULT_FRAC_DUR,
    normalize=True):
    """Calls both make_pval_timeseries and timeseries_to_stacked
    """
    wd_subsys = os.path.join(wd, "{}_{}".format(ifo, subsys))
    subsys_channels = os.path.join(wd_subsys, "channels.txt")

    make_pval_timeseries(
        ifo, wd_subsys, tagname, gps_groups, subsys_channels,
        trailing_frametype=trailing_frametype,
        pp_target_signif=pp_target_signif,
        pp_hoft_surrogate=pp_hoft_surrogate,
        pp_srate=pp_srate, pp_active_signif=pp_active_signif,
        pp_thresholds=pp_thresholds, pp_window=pp_window,
        pp_left_rate_estimation_window=pp_left_rate_estimation_window,
        pp_right_rate_estimation_window=pp_right_rate_estimation_window,
        frac_dur=frac_dur
                        )
    timeseries_to_stacked(
        ifo, wd_subsys, tagname, gps_groups, normalize=normalize)


def stacked_to_chanlists(ifo, wd, tagname,
                         danger_pval=DEFAULT_PVALUE_DANGER,
                         warning_pval=DEFAULT_PVALUE_WARNING):
    """Go from stacked pvalues to lists of ok, warning, and danger channels.
    Assumes that all stacked_*.npy channels within the working directory
    including the tagname are to be included in these channel lists.

    Parameters
    ---------
    ifo: str
        "H1" or "L1"
    wd: str
        Path to working directory
    tagname: str
        Tagname for this pointy run

    Returns
    -------
    None, but the files should have been written out
    """
    # Get stacked npys
    stacked_npys = glob.glob(os.path.join(
        wd, "{i}_*/stacked_{t}/stacked_*{i}*{t}*.npy".format(
            i=ifo, t=tagname)))
    stacked_npys = [os.path.abspath(path) for path in stacked_npys]

    outdir = os.path.join(wd, "chanlists_{}".format(tagname))

    chan_npy_pairs = ["{},{}".format(
        re.search('stacked_(.*)_{}'.format(tagname),
                  os.path.basename(npy)).group(1), npy)
        for npy in stacked_npys]

    # break pairs into manageable sizes (lists of len = 100 each)
    n = 100
    chan_npy_pairs_broken = [chan_npy_pairs[i*n : (i+1)*n]
                             for i in range(
                                 (len(chan_npy_pairs) + n - 1) // n)]
    for i, chunk in enumerate(chan_npy_pairs_broken):
        # make temporary directory
        tempdirpath = os.path.join(outdir, "temp{}".format(i))
        try:
            os.makedirs(tempdirpath)
        except OSError:
            pass
        # string together command arguments
        chan_npy_pairs_str = " ".join(chunk)
        command = ("logpvalue2chans -v"
                   " --max-pvalue-danger {} --max-pvalue-warning {}"
                   " --output-dir {} {}").format(
                       danger_pval, warning_pval,
                       tempdirpath, chan_npy_pairs_str)
        # actually run the thing
        os.system(command)

    # cat results and clean up dirs
    for resulttype in ["ok", "danger", "warning"]:
        os.system(("cat {outdir}/temp*/logpvalue2chans-{restype}* > "
                   "{outdir}/logpvalue2chans-{restype}.txt").format(
                       outdir=outdir, restype=resulttype))
    os.system("rm -r {}/temp*".format(outdir))


def stacked_to_onelist(ifo, wd, tagname):
    """Go from stacked pvalues to one list of channels and pvals.
    Assumes that all stacked_*.npy channels within the working directory
    including the tagname are to be included in these channel lists.
    Writes a list of these pvals to the wd/chanlists_tagname.

    Parameters
    ---------
    ifo: str
        "H1" or "L1"
    wd: str
        Path to working directory
    tagname: str
        Tagname for this pointy run

    Returns
    -------
    list of (str, float)
        List of channels and accompanying p-vals
    """
    # Get stacked npys
    print('stacked_to_onelist is running')
    stacked_npys = glob.glob(os.path.join(
        wd, "{i}_*/stacked_{t}/stacked_*{i}*{t}*.npy".format(
            i=ifo, t=tagname)))
    stacked_npys = [os.path.abspath(path) for path in stacked_npys]

    outdir = os.path.join(wd, "chanlists_{}".format(tagname))
    try:
        os.mkdir(outdir)
    except OSError: # directory already exists
        pass

    chans_pvals = [
        (re.search('stacked_(.*)_{}'.format(tagname), os.path.basename(npy)).group(1),
         float(np.load(npy)))
        for npy in stacked_npys]

    chan_pval_dict = {}
    for chan, pval in chans_pvals:
        if not chan_pval_dict.get(chan):
            chan_pval_dict[chan] = pval
        else:
            existing_pval = chan_pval_dict[chan]
            if existing_pval > pval:
                chan_pval_dict[chan] = pval

    chans_pvals_sorted = sorted(chan_pval_dict.items(), key=lambda x: x[1])

    with open(os.path.join(outdir, 'kwchans_pvals_all.txt'), 'w+') as f:
        for chan, pval in chans_pvals_sorted:
            f.write(chan + ' ' + str(pval) + '\n')

    all_lpvls = np.array([pval for chan, pval in chans_pvals_sorted])
    np.save(os.path.join(outdir, 'kwchans_pvals_all.npy'), all_lpvls)

    return chans_pvals_sorted


def offsource_distribution(ifo, wd, tagname, t_start, t_end, N,
                           normalize=False, stack=False, cdf=True, plot=True,
                           cumulative=False, lpvls_npy='', save_times=False,
                           only_good_times=True, tex=False):
    """Draw offsource distribution and make a plot. Assumes that timeseries
    npys are separated into their respective subsystems under the wd, and
    are in directories inside the subsystems titled timeseries_*

    lpvls_npy: str, optional
        if given, skips drawing times and lpvls and uses the given path to
        a lpvls.npy

    Returns: offsource lplvls
    """
    from pointy import utils as putils
    from pointy import pointy as ppointy

    timeseries_list = glob.glob(
        os.path.join(wd, ifo + "_*", "timeseries*/*.npy"))
    offsource_dir = os.path.join(wd, "offsource")
    try:
        os.mkdir(offsource_dir)
    except OSError:
        pass

    if not lpvls_npy:
        if only_good_times:
            times = utils.draw_random_valid_times(
                ifo, offsource_dir, t_start, t_end, N)
        else:
            times = np.random.uniform(t_start, t_end, N)
        if save_times:
            np.save(os.path.join(offsource_dir, "offsource_cdf_times.npy"),
                    times)

        lpvls = []
        for path in timeseries_list:
            ts_lpvls = []
            array, s, d = putils.load(path, verbose=True)
            lpvl = array['logpvalue']
            for gps in times:
                this_lpvl = np.interp(
                    gps, np.arange(s, s+d, 1.*d/len(lpvl)), lpvl)
                ts_lpvls.append(this_lpvl)

            if stack:
                ans = ppointy.lognaivebayes(ts_lpvls)
                if normalize:
                    ans /= 1.*len(ts_lpvls)
                ans_list = [ans]
            else:
                ans_list = ts_lpvls
            lpvls.extend(ans_list)

        np.save(os.path.join(offsource_dir, "lpvls.npy"), lpvls)

    else: # use given lpvls.npy
        lpvls = np.load(lpvls_npy)

    if plot:
        stackedstr = "_stacked" if stack else ""
        normedstr = "_normalized" if normalize else ""
        cdfstr = "_cdf" if cdf else ""
        filename = tagname + "_offsource_distribution" + stackedstr +\
                normedstr + cdfstr + ".png"
        data_dict = utils.plot_pvalues(
            [('offsource', lpvls)],
            "Log p-values from {} random offsource times\n{} {}".format(
                N, ifo, tagname),
            os.path.join(offsource_dir, filename),
            cdf=cdf, cumulative=cumulative, meaned=normalize, tex=tex)
        for name, data in data_dict.items():
            dataname = "cdf.npy" if cdf else "hist.npy"
            np.save(os.path.join(offsource_dir, name + '_' + dataname), data)

    return lpvls

def recalc_chanlists_from_timeseries(
    ifo, wd, tagname, gps_groups, normalize=True,
    danger_pval=DEFAULT_PVALUE_DANGER, warning_pval=DEFAULT_PVALUE_WARNING):
    """Runs in main working directory. Assumes timeseries exist in their
    respective subsystem directories.

    Parameters
    ---------
    ifo: str
        "H1" or "L1"
    wd: str
        Path to working directory. This should be the parent of the subsys
        directories.
    tagname: str
        Tagname for this pointy run
    gps_groups: list of tuples
        List of tuples (description, path to gpstimes.txt).
        For example, [('SNR100_100Hz', '/path/to/snr100_100hz.txt'),
                      ('ALL', '/path/to/all.txt')]
    normalize: bool, optional
        Take the average of pvalues instead of just the sum

    """
    subsys_dirs = glob.glob(os.path.join(wd, "{}_*/".format(ifo)))
    for subsys_wd in subsys_dirs:
        timeseries_to_stacked(ifo, subsys_wd, tagname, gps_groups,
                              normalize=normalize)
    stacked_to_chanlists(ifo, wd, tagname, danger_pval=danger_pval,
                         warning_pval=warning_pval)


def add_fap_to_chanlist(chanlists, cdf_npy, outdir,
                        fap_danger=0.01, fap_warning=0.05):
    if type(chanlists) == str:
        chanlists = [chanlists]
    chan_pval = {}
    for chanlist in chanlists:
        chan_pval.update(np.loadtxt(chanlist, dtype='|S100,float'))
    cdf = np.load(cdf_npy)

    chan_pval_fap = {chan: (pval, np.interp(pval, *cdf))
                     for chan, pval in chan_pval.items()}
    ok = {chan: stats for chan, stats in chan_pval_fap.items()
          if stats[-1] > fap_warning}
    warning = {chan: stats for chan, stats in chan_pval_fap.items()
               if fap_danger < stats[-1] <= fap_warning}
    danger = {chan: stats for chan, stats in chan_pval_fap.items()
              if stats[-1] <= fap_danger}
    for chandict, name in [(ok, 'ok'), (warning, 'warning'),
                           (danger, 'danger')]:
        chandictlist = chandict.items()
        chandictlist = [(channame, stats[0], stats[1])
                        for channame, stats in chandictlist]
        chandictlist_sorted = np.array(
            sorted(chandictlist, key=lambda x: x[-1]))
        np.savetxt(os.path.join(outdir, "chanlist-{}.txt".format(name)),
                   chandictlist_sorted, header="chan pval fap", fmt='%s')


def autocorrelogram(channels, rundir, outdir, filename,
                randomtimes=0, Nrandomtimes=0, tstart=0, tend=0,
                bins=20, tlimit=None, tcluster=0,
                ylog=False, xlog=False, absval=True,
                listoftimes=None, matchNforRandom=False):

    if type(channels) is not list:
        channels = [channels]

    chans_trgtimes = {channel:
                      [trg[2] for trg in utils.find_kwtrgs_for_channel(
                        rundir, channel)]
                     for channel in channels}
    vals_labels = [
        (utils.correlogram_diffs(trgtimes, tlimit=tlimit, absval=absval,
                                 tcluster=tcluster),
         channel, None, 1)
        for channel, trgtimes in chans_trgtimes.items()]

    if randomtimes:
        if matchNforRandom:
            randomtimes = int(np.mean([len(times) for times in
                                       chans_trgtimes.values()]))
        rand_trgtimes = [np.random.uniform(tstart, tend, randomtimes)
                         for i in range(Nrandomtimes)]
        vals_labels.append((
            utils.correlogram_diffs(rand_trgtimes[0], tlimit=tlimit,
                                    absval=absval, tcluster=tcluster),
            '{} samples of {} random times'.format(
                Nrandomtimes, randomtimes), 'grey', 0.2))
        for rand_trgtime in rand_trgtimes[1:]:
            vals_labels.append((
                utils.correlogram_diffs(
                    rand_trgtime, tlimit=tlimit, absval=absval,
                    tcluster=tcluster),
                None, 'grey', 0.3))

    utils.correlogram_plot(vals_labels, os.path.join(outdir, filename),
                           '$\Delta t$ [sec]', 'counts', 'autocorrelogram',
                          bins=bins)


def crosscorrelogram(channel1, channel2, rundir, outdir, filename,
                randomtimes=0, Nrandomtimes=0, tstart=0, tend=0,
                bins=20, tlimit=None, tcluster=0,
                ylog=False, xlog=False, absval=True,
                listoftimes=None, matchNforRandom=False):
    channel1_trgtimes = [
        trg[2] for trg in utils.find_kwtrgs_for_channel(rundir, channel1)]
    channel2_trgtimes = [
        trg[2] for trg in utils.find_kwtrgs_for_channel(rundir, channel2)]

    vals_labels = [(
        utils.correlogram_diffs(
            channel1_trgtimes, times2=channel2_trgtimes,
            tlimit=tlimit, absval=absval, tcluster=tcluster),
        channel1 + ',\n' + channel2, None, 1)]

    if randomtimes:
        if matchNforRandom:
            randomtimes1 = len(channel1_trgtimes)
            randomtimes2 = len(channel2_trgtimes)
        else:
            randomtimes1 = randomtimes
            randomtimes2 = randomtimes
        rand_trgtimes1 = [np.random.uniform(tstart, tend, randomtimes1)
                          for i in range(Nrandomtimes)]
        rand_trgtimes2 = [np.random.uniform(tstart, tend, randomtimes2)
                          for i in range(Nrandomtimes)]
        vals_labels.append((
            utils.correlogram_diffs(
                rand_trgtimes1[0], times2=rand_trgtimes2[0],
                tlimit=tlimit, absval=absval, tcluster=tcluster),
            '{} samples of {}, {} random times'.format(
                Nrandomtimes, randomtimes1, randomtimes2), 'grey', 0.2))
        for rand_trgtime1, rand_trgtime2 in zip(
            rand_trgtimes1[1:], rand_trgtimes2[1:]):
            vals_labels.append((
                utils.correlogram_diffs(
                    rand_trgtime1, times2=rand_trgtime2,
                    tlimit=tlimit, absval=absval, tcluster=tcluster),
                None, 'grey', 0.3))

    utils.correlogram_plot(vals_labels, os.path.join(outdir, filename),
                           '$\Delta t$ [sec]', 'counts', 'crosscorrelogram',
                          bins=bins)


def offsource_distribution_N_BBHs(ifo, wd, tagname, N, M_bbh, outdir,
                                  times_npy, verbose=False, error=True):
    """Draw M_bbh offsource distributions with N p-values.
    Uses M_bbh*N p-values drawn from the array of vetted times given in
    times_npy. Takes the mean of M_bbh p-values, so that each cdf is
    made with N p-values. Saves each set of lpvls and its associated
    cdf to the outdir directory.
    """
    from pointy import utils as putils
    from pointy import pointy as ppointy

    outdir = os.path.abspath(outdir)

    # load channels
    timeseries_list = glob.glob(
        os.path.join(wd, ifo + "_*", "timeseries*/*.npy"))
    timeseriess = [putils.load(path, verbose=verbose)
                   for path in timeseries_list]

    available_times = np.load(times_npy)

    cdfs = []
    for M in M_bbh:
        M_outdir = os.path.join(outdir, '{}_background'.format(M))
        try:
            os.mkdir(M_outdir)
        except OSError:
            pass

        # draw and save times
        times = np.random.choice(available_times, M*N, replace=False)
        np.save(os.path.join(M_outdir, str(M) + '_times.npy'), times)

        # pick out pvalues for those times
        lpvls = []
        for array, s, d  in timeseriess:
            lpvl = array['logpvalue']
            gps_array = np.arange(s, s+d, 1.*d/len(lpvl))

            ts_lpvls = []
            for gps in times:
                this_lpvl = np.interp(gps, gps_array, lpvl)
                ts_lpvls.append(this_lpvl)
            lpvls.extend(ts_lpvls)
        lpvls = np.array(lpvls)
        np.save(os.path.join(M_outdir, str(M) + '_lpvls.npy'), lpvls)

        # mean M pvalues and save pvalues
        meaned_lpvls = np.mean(lpvls.reshape(-1, M), axis=1)
        np.save(os.path.join(M_outdir, str(M) + '_meaned_lpvls.npy'), meaned_lpvls)

        # make and save cdf
        cdf = utils.make_cdf(meaned_lpvls)
        np.save(os.path.join(M_outdir, str(M) + '_cdf.npy'), cdf)
        cdfs.append((M, cdf))

    # plot cdfs
    plt.rcParams['text.usetex'] = True
    figwidth = 7
    figheight = 5
    dpi = 500

    plt.figure(figsize=(figwidth, figheight))
    for M, cdf in cdfs:
        plt.plot(*cdf, label=str(M) + '-stacked', lw=0.75)
        plt.ylabel('CDF of channels')
    plt.yscale('log')
    plt.xlabel('$\mu_{\log{p}}$')
    plt.legend(loc='best')
    plt.grid(True, which='both', linestyle=':')
    filename = 'stacked_bbh_{}_times_background_cdfs.png'.format(N)
    plotpath = os.path.join(outdir, filename)
    plt.savefig(plotpath, dpi=dpi)
    plt.close()


def compare_channel_list(channel_list_ini, pointy_safe_list, pointy_all_list,
                         outname_compared=None, outname_full_list=None, raw_frames=True):
    '''Compares a detchar channel list to the pointy safe list.
    Returns a subset of the detchar channel list which was
    - not found in the safe list
    - unsafe channels found in the pointy safe list

    Parameters
    ----------
    channel_list_ini : str
        Path to detchar channel list (e.g., H1-O4-standard.ini)
    pointy_safe_list : str
        Path to pointy safe list with regular (non-KW) channel names
    pointy_all_list : str
        Path to pointy all list with regular (non-KW) channel names
    outname_compared : str, optional
        Filename to write a compared summary to.
    outname_full_list : str, optional
        Filename to write a full csv of pointy channels,
        with
            channel, logpval, pointy safety, channel_list
        as columns
    raw_frames : bool, optional
        Only look at channel list channels in H1_R or L1_R frames.

    Returns
    -------
    list of str
        List of above channels
    '''
    import configparser
    config = configparser.ConfigParser()
    config.read(channel_list_ini)

    ini_chans_safe = []
    ini_chans_unsafe = []
    ini_chans_other = []

    for sec in config.sections():
        if raw_frames:
            if config.get(sec, 'frametype').endswith('_R'):
                unparsed = config.get(sec, 'channels')
                parsed = unparsed.splitlines()[1:]

                for chan in parsed:
                    channame, hz, safety, cleaned = chan.split()
                    if safety == 'unsafe':
                        ini_chans_unsafe.append(channame)
                    elif safety == 'safe':
                        ini_chans_safe.append(channame)
                    else:
                        ini_chans_other.append(channame)

    ini_chans_all = ini_chans_safe + ini_chans_unsafe + ini_chans_other

    with open(pointy_safe_list, 'r') as f:
        unparsed = f.read().splitlines()
    pointy_safe_with_pvals = [chan.split(' ') for chan in unparsed]
    pointy_safe = [chan[0] for chan in pointy_safe_with_pvals]

    with open(pointy_all_list, 'r') as f:
        unparsed = f.read().splitlines()
    pointy_all_with_pvals = [chan.split(' ') for chan in unparsed]
    pointy_all = [chan[0] for chan in pointy_all_with_pvals]
    pointy_unsafe = set(pointy_all) - set(pointy_safe)

    # we should account for all the channels in both lists

    # ini channels that don't exist in pointy: safe
    ini_not_in_pointy = sorted(set(ini_chans_all) - set(pointy_all))
    # ini safe channels that are in pointy safe: safe
    ini_safe_pointy_safe = sorted([
        chan for chan in (set(ini_chans_safe) - set(ini_not_in_pointy))
        if chan in set(pointy_safe)])
    # ini safe channels that are in pointy unsafe: check
    ini_safe_pointy_unsafe = sorted([
        chan for chan in (set(ini_chans_safe) - set(ini_not_in_pointy))
        if chan in set(pointy_unsafe)])
    # ini unsafe channels that are in pointy safe: check
    ini_unsafe_pointy_safe = sorted([
        chan for chan in (set(ini_chans_unsafe))
        if chan in set(pointy_safe)])
    # ini unsafe channels that are in pointy unsafe: check
    ini_unsafe_pointy_unsafe = sorted([
        chan for chan in (set(ini_chans_unsafe))
        if chan in set(pointy_unsafe)])
    # these should add up to the total number of ini channels
    sanity_ini = len(ini_not_in_pointy) + len(ini_safe_pointy_safe) + len(ini_safe_pointy_unsafe) \
        + len(ini_unsafe_pointy_safe) + len(ini_unsafe_pointy_unsafe)
    print("ini channels that don't exist in pointy: {}".format(len(ini_not_in_pointy)))
    print("ini safe channels that are in pointy safe: {}".format(len(ini_safe_pointy_safe)))
    print("ini safe channels that are in pointy unsafe: {}".format(len(ini_safe_pointy_unsafe)))
    print("ini unsafe channels that are in pointy safe: {}".format(len(ini_unsafe_pointy_safe)))
    print("ini unsafe channels that are in pointy unsafe: {}".format(len(ini_unsafe_pointy_unsafe)))
    print("sum of above: {}".format(sanity_ini))
    print("total ini chans: {}\n".format(len(ini_chans_all)))

    # pointy channels
    # pointy safe not in ini: safe
    pointy_safe_not_in_ini = sorted(set(pointy_safe) - set(ini_chans_all))
    # pointy unsafe not in ini: check
    pointy_unsafe_not_in_ini = sorted(set(pointy_unsafe) - set(ini_chans_all))
    # pointy safe in ini as safe: safe
    pointy_safe_ini_safe = sorted([
        chan for chan in pointy_safe if chan in ini_chans_safe
    ])
    # pointy unsafe in ini as safe: check
    pointy_unsafe_ini_safe = sorted([
        chan for chan in pointy_unsafe if chan in ini_chans_safe
    ])
    # pointy safe in ini as unsafe: check
    pointy_safe_ini_unsafe = sorted([
        chan for chan in pointy_safe if chan in ini_chans_unsafe
    ])
    # pointy unsafe in ini as unsafe: check
    pointy_unsafe_ini_unsafe = sorted([
        chan for chan in pointy_unsafe if chan in ini_chans_unsafe
    ])
    # these should add up to total number of pointy channels
    sanity_pointy = len(pointy_safe_not_in_ini) + len(pointy_unsafe_not_in_ini) \
        + len(pointy_safe_ini_safe) + len(pointy_unsafe_ini_safe) \
        + len(pointy_safe_ini_unsafe) + len(pointy_unsafe_ini_unsafe)
    print("pointy safe not in ini: {}".format(len(pointy_safe_not_in_ini)))
    print("pointy unsafe not in ini: {}".format(len(pointy_unsafe_not_in_ini)))
    print("pointy safe in ini as safe: {}".format(len(pointy_safe_ini_safe)))
    print("pointy unsafe in ini as safe: {}".format(len(pointy_unsafe_ini_safe)))
    print("pointy safe in ini as unsafe: {}".format(len(pointy_safe_ini_unsafe)))
    print("pointy unsafe in ini as unsafe: {}".format(len(pointy_unsafe_ini_unsafe)))
    print("sum of above: {}".format(sanity_pointy))
    print("total pointy chans: {}".format(len(pointy_all)))

    if outname_compared:
        with open(outname_compared, 'w+') as f:
            f.write('# ini file: {}\n'.format(channel_list_ini))
            f.write('# pointy ok list file: {}\n'.format(pointy_safe_list))
            f.write("\n")

            f.write('summary statistics:\n')
            f.write("ini channels that don't exist in pointy: {}\n".format(len(ini_not_in_pointy)))
            f.write("ini safe channels that are in pointy safe: {}\n".format(len(ini_safe_pointy_safe)))
            f.write("ini safe channels that are in pointy unsafe: {}\n".format(len(ini_safe_pointy_unsafe)))
            f.write("ini unsafe channels that are in pointy safe: {}\n".format(len(ini_unsafe_pointy_safe)))
            f.write("ini unsafe channels that are in pointy unsafe: {}\n".format(len(ini_unsafe_pointy_unsafe)))
            f.write("sum of above: {}\n".format(sanity_ini))
            f.write("total ini chans: {}\n".format(len(ini_chans_all)))
            f.write("\n")
            f.write("pointy safe not in ini: {}\n".format(len(pointy_safe_not_in_ini)))
            f.write("pointy unsafe not in ini: {}\n".format(len(pointy_unsafe_not_in_ini)))
            f.write("pointy safe in ini as safe: {}\n".format(len(pointy_safe_ini_safe)))
            f.write("pointy unsafe in ini as safe: {}\n".format(len(pointy_unsafe_ini_safe)))
            f.write("pointy safe in ini as unsafe: {}\n".format(len(pointy_safe_ini_unsafe)))
            f.write("pointy unsafe in ini as unsafe: {}\n".format(len(pointy_unsafe_ini_unsafe)))
            f.write("sum of above: {}\n".format(sanity_pointy))
            f.write("total pointy chans: {}\n".format(len(pointy_all)))
            f.write("\n")

            f.write("# ini safe channels that are in pointy unsafe == pointy unsafe in ini as safe\n{}\n\n".format('\n'.join(ini_safe_pointy_unsafe)))
            f.write("# ini unsafe channels that are in pointy safe == pointy safe in ini as unsafe\n{}\n\n".format('\n'.join(ini_unsafe_pointy_safe)))
            f.write("# ini unsafe channels that are in pointy unsafe == pointy unsafe in ini as unsafe \n{}\n\n".format('\n'.join(ini_unsafe_pointy_unsafe)))
            f.write("\n")

            f.write("# pointy unsafe not in ini \n{}\n\n".format('\n'.join(pointy_unsafe_not_in_ini)))

    if outname_full_list:
        # FIXME: this is lazy and inefficient code
        outlines = []
        for line in pointy_all_with_pvals:
            row = line
            channel = row[0]
            if channel in pointy_unsafe:
                row.append('unsafe')
            else:
                row.append('safe')
            if channel in ini_chans_safe:
                row.append('safe')
            elif channel in ini_chans_unsafe:
                row.append('unsafe')
            else:
                row.append(' ')
            row.append('\n')
            outlines.append(row)

        with open(outname_full_list, 'w+') as f:
            f.write('#channel,log(pval),pointy safety,channel list safety\n')
            for line in outlines:
                f.write(','.join(line))


    return
