import argparse
import glob
import os
import sys

import safety
import utils

"""
This script is designed to make pvalue timeseries and stack them for
a given ifo, tagname, and list of subsystems, assuming the "main" pipeline.py
has already split into subsystems.

The script was written to "rescue" subsystems for which nothing seems to be
happening in pipeline.py.
"""

parser = argparse.ArgumentParser()
parser.add_argument('ifo', type=str, help='ifo to analyze ("H1" or "L1")')
parser.add_argument('wd', type=str, help='working directory for this run')
parser.add_argument('tagname', type=str, help='tagname for this run')
parser.add_argument('subsystems', type=str, nargs='+',
                    help='subsystems to run')

args = parser.parse_args()
ifo = args.ifo
wd = os.path.abspath(args.wd)
tagname = args.tagname
subsys_list = args.subsystems

sys.stdout = open(
    os.path.join(wd, "{}_{}_rescue.out".format(ifo, tagname)), "w+")
sys.stderr = open(
    os.path.join(wd, "{}_{}_rescue.err".format(ifo, tagname)), "w+")

kw_all = os.path.abspath(glob.glob(
    os.path.join(wd, '{}_{}-ALL*').format(ifo, tagname))[0])

gps_groups = [('ALL', kw_all)]

async_args_tuple = tuple([
    tuple([ifo, wd, tagname, subsys, gps_groups]) for subsys in subsys_list])
utils.do_asynchronously(
    safety.make_pval_timeseries_to_stacked_for_subsys, async_args_tuple)
