import csv
import glob
import logging
import multiprocessing as mp
import os
from textwrap import dedent

from gwdatafind import find_urls
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

import safety

# ---- settings ----
DEFAULT_BUFFER = 10  # seconds around main time to query datafind
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=os.environ.get("LOGLEVEL", "INFO"))

# -- kw settings ---
DEFAULT_STRIDE = 16
DEFAULT_BASENAME = "-KW_TRIGGERS"
DEFAULT_SEGNAME = "-KW_SEGMENTS"
DEFAULT_SIGNIFICANCE = 15.0
DEFAULT_THRESHOLD = 3.0
DEFAULT_DECIMATEFACTOR = -1
DEFAULT_TRAILING_FRAMETYPE = '_R'
DEFAULT_CHANNEL = "CAL-DELTAL_EXTERNAL_DQ"
DEFAULT_F_LOW = 16.0
DEFAULT_F_HIGH = 2048.0

DEFAULT_KW_WINDOW = 0.1


# ---- T1900555 parameters ---
# freq, snr pairs for each ifo based on T1900555
INJ_PAIRS = {
    'H1': [(20, 15), (20, 50), (20, 100), (20, 250), (20, 500),
           (60, 15), (60, 50), (60, 100), (60, 250), (60, 500),
           (100, 15), (100, 50), (100, 100), (100, 250), (100, 500),
           (180, 15), (180, 50), (180, 100), (180, 250),
           (260, 15), (260, 50), (260, 100),
           (350, 15), (350, 50),
           (500, 15)],
    'L1': [(20, 15), (20, 50), (20, 100), (20, 250), (20, 500),
           (60, 15), (60, 50), (60, 100), (60, 250), (60, 500),
           (100, 15), (100, 50), (100, 100), (100, 250), (100, 500),
           (180, 15), (180, 50), (180, 100), (180, 250),
           (260, 15), (260, 50), (260, 100),
           (350, 15), (350, 50), (350, 100),
           (500, 15), (500, 50),
           (700, 15)]
}

INJ_PADDING = 10  # seconds after t_start before first inj
INJ_BUFFER = 5  # seconds between injections
INJ_REPEATS = 3  # repeats of each freq, snr pair


def find_frames_to_txt(ifo, t_start, t_end,
                       trailing_frametype=DEFAULT_TRAILING_FRAMETYPE):
    """Find frames and get them into the form KW wants

    Parameters
    ----------
    ifo: str
        "H1" or "L1"
    t_start: float or int
        When to start looking for frames
    t_end: float or int
        When to stop looking for frames
    trailing_frametype: str, optional
        Type of frame to look for, without the ifo prefix, eg. '_R'

    Returns
    -------
    list of strs : paths to frames in KW format
    """
    frames = find_urls(ifo[0], ifo + trailing_frametype, t_start, t_end)
    frames_kw_format = []
    for frame in frames:
        frames_kw_format.append(frame.split('localhost')[-1])
    return frames_kw_format


def write_kw_frames_to_txt(ifo, t_start, t_end, wd,
                       trailing_frametype=DEFAULT_TRAILING_FRAMETYPE):
    """Find frames with find_frames_to_txt, format them in the way KW likes,
    and write them to ifo_t_start_to_t_end_kw_frames.txt,
    one line per frame

    Parameters
    ----------
    ifo: str
        "H1" or "L1"
    t_start: float or int
        When to start looking for frames
    t_end: float or int
        When to stop looking for frames
    wd: str
        Path to working directory, where the file will be written
    trailing_frametype: str, optional
        Type of frame to look for, without the ifo prefix, eg. '_R'

    Returns
    -------
    str: path to written out txt file with KW names
    """
    frames_kw_format = find_frames_to_txt(ifo, t_start, t_end,
                                          trailing_frametype)
    pathname = os.path.join(wd, "{}_{}_to_{}_kw_frames.txt".format(
        ifo, int(t_start), int(t_end)))
    with open(pathname, "w+") as f:
        f.write('\n'.join(frames_kw_format))
        f.write('\n')
    return pathname


def build_kw_config(ifo, wd,
                    stride=DEFAULT_STRIDE, basename=DEFAULT_BASENAME,
                    segname=DEFAULT_SEGNAME, significance=DEFAULT_SIGNIFICANCE,
                    threshold=DEFAULT_THRESHOLD,
                    decimateFactor=DEFAULT_DECIMATEFACTOR,
                    channel=DEFAULT_CHANNEL,
                    f_low=DEFAULT_F_LOW, f_high=DEFAULT_F_HIGH):
    """Build a KleineWelle config file and write it to IFO-KW_STRAIN.cfg

    Parameters
    ---------
    ifo: str
        "H1" or "L1"
    wd: str
        Path to working directory, where the file will be written
    other parameters: strs, optional
        KW parameters

    Returns
    -------
    str: path to written out KW cfg file
    """
    pathname = os.path.join(wd, "{}-KW_STRAIN.cfg".format(ifo))
    with open(pathname, "w+") as f:
        f.write("stride {}\n".format(stride))
        f.write("basename {}{}\n".format(ifo[0], basename))
        f.write("segname {}{}\n".format(ifo[0], segname))
        f.write("significance {}\n".format(significance))
        f.write("threshold {}\n".format(threshold))
        f.write("decimateFactor {}\n".format(decimateFactor))
        f.write("channel {}:{} {} {}\n".format(
            ifo, channel, f_low, f_high))
    return pathname


def run_kw(ifo, wd, t_start, t_end):
    """Run kleineWelle assuming there is already a frame and config txt file
    in the working directory

    NOTE: this takes t_start[:5] in various places. It's a possibility that
    we could eventually have a t_start like 1000099999, which could flow over into
    1000100000 which would break this code. Be aware!

    Parameters
    ----------
    ifo: str
        "H1" or "L1"
    wd: str
        Path to working directory, where the file will be written
    t_start: float or int
        When to start running KW
    t_end: float or int
        When to stop running KW

    Returns
    -------
    str: path to created "IFO-KW_TRIGGERS_ALL.trg" file, including all
         triggers within the specified time
    """
    cmd = "cd {} && kleineWelleM {} -inlist {}".format(
        wd,
        os.path.join(wd, "{}-KW_STRAIN.cfg".format(ifo)),
        os.path.join(wd, "{}_{}_to_{}_kw_frames.txt".format(
        ifo, int(t_start), int(t_end))))
    os.system(cmd)

    created_trigger_files = glob.glob(
        os.path.join(
            wd, "{}-KW_TRIGGERS-{}".format(ifo[0], str(t_start)[:5]),
            "{}-KW_TRIGGERS-{}*-*.trg".format(ifo[0], str(t_start)[:5])
        )
    )

    alltrgs = os.path.join(
        wd,
        "{}-KW_TRIGGERS-{}".format(ifo[0], str(t_start)[:5]),
        "{}-KW_TRIGGERS-ALL.trg".format(ifo[0]))
    header = ("# start_time stop_time time frequency unnormalized_energy "
              "normalized_energy chisqdof significance channel\n")
    with open(alltrgs, 'w+') as f:
        f.write(header)
        for trg in created_trigger_files:
            with open(trg, 'r') as r:
                for line in r:
                    if 'start_time' not in line:
                        f.write(line)

    return alltrgs


def load_kwtrgs(kwdir):
    kwdir_files = glob.glob(os.path.join(kwdir, "*KW_*TRIGGERS*.trg"))
    kwtrg_arrays = [
        np.loadtxt(
    f, dtype='float,float,float,float,float,float,float,float,|S100')
        for f in kwdir_files]
    kwtrgs = np.concatenate(kwtrg_arrays)
    return kwtrgs


def find_kwtrgs_for_channel(rundir, kw_channame):
    ifo = kw_channame[:2]
    subsys_dir = os.path.join(rundir, kw_channame[:6])
    kwdir = os.path.join(
        subsys_dir,
        '{0}-KW_{1}*RDS*_TRIGGERS/{0}-KW_{1}*RDS*TRIGGERS*/'.format(
            ifo[0], ifo))
    all_kwtrgs = load_kwtrgs(kwdir)
    return np.array([row for row in all_kwtrgs if row[-1] == kw_channame])


def find_timeseries_for_channel(rundir, channel):
    subsys = channel[:6]
    globbed = glob.glob(os.path.join(
        rundir, subsys, 'timeseries_*', channel + '*.npy'))
    if len(globbed) > 1:
        raise NameError('more than one channel timeseries was found')
    return globbed[0]


def match_kw_time(dialed_times, kwfile, window=DEFAULT_KW_WINDOW):
    """Match KW picked-up times with the dialed times.

    Parameters
    ----------
    dialed_times : list of ints or floats
        List of dialed times, in order
    kwfile : str
        Path to KW triggers file
    widnow : int or float, optional
        Width of symmetric window about dialed time around which we consider
        a KW trigger "found"

    Returns
    -------
    list
        List of floats corresponding to matched KW time, or None if not matched
    """
    kw_trgs = []
    picked_up_times = []
    with open(kwfile, 'r') as f:
        rows = f.readlines()
        for row in rows:
            if row[0] == '#':
                continue
            trg = float(row.split()[2])
            kw_trgs.append(trg)
    for dialed in dialed_times:
        candidates = []
        for kw_trg in kw_trgs:
            dt = abs(dialed - kw_trg)
            candidates.append((kw_trg, dt))
        candidates.sort(key=lambda x: x[1])
        can = candidates[0]
        if can[1] < window:
            picked_up_times.append(np.float64(can[0]))
        else:
            picked_up_times.append(None)
    return picked_up_times


def inj_t1900555_to_txt(ifo, t_start, tagname, outputdir='inj_times',
                        pairs=INJ_PAIRS, padding=INJ_PADDING,
                        buffer=INJ_BUFFER, repeats=INJ_REPEATS):
    """Outputs files named with the frequency and expected SNR based on
    the IFO and the T1900555 hardware injection specifications.
    These files contain the gpstimes of the injections with that frequency,
    SNR pair.

    Parameters
    ----------
    ifo: str
        "H1" or "L1"
    t_start: int or float
        Time of injection waveform beginning (not necessarily the time of
        the first injection, note the buffer
    tagname: str
        String to describe this injection set (note that ifo will always be
        noted, so this doesn't have to include the ifo)
    outputdir: str
        Path to where these files should be output
    pairs: dict, optional
        Dict of ifo:list of injset pairs, where the inject pairs
        correspond to (frequency, SNR). For example,
        INJ_PAIRS = {
            'H1': [(20, 15), (20, 500), (60, 250)],
            'L1': [(500, 15), (500, 500), (700, 50)]
            }
    padding: int or float, optional
        Time (s) between the start of the injected waveform and the first
        injection, and  between the last injection and the end of the waveform
    buffer: int or float, optional
        Time (s) between each injection. The injection time is negligible.
    repeats: int, optional
        How many times each injset pair is repeated.

    Returns
    -------
    str: path to IFO_TAGNAME-ALL-T_START.txt, with KW times of all injections
         in one file
    """
    if ifo not in ['H1', 'L1']:
        raise ValueError('Please enter either H1 or L1 as the ifo')

    if os.path.isabs(outputdir):
        pass
    else:
        outputdir = os.path.join(os.getcwd(), outputdir)
    try:
        os.mkdir(outputdir)
    except OSError:
        pass

    dialed_times = [t_start + padding + i*buffer
                    for i in range(repeats*len(pairs[ifo]))]

    # Find KW frames and match KW times to dialed ones
    t_end = max(dialed_times) + buffer
    kw_frames = write_kw_frames_to_txt(ifo, t_start, t_end, outputdir)
    kw_config = build_kw_config(ifo, outputdir)
    kw_trgfile = run_kw(ifo, outputdir, t_start, t_end)
    pickedup_times = match_kw_time(dialed_times, kw_trgfile)
    # Make sure the number of dialed things == number of searched-for things
    if len(dialed_times) != len(pickedup_times):
        raise ValueError(('Number of dialed things is not the same as number '
                          'of searched-for things in KW'))
    # this filename has details in order ifo, tagname, freq, snr, t0
    raw_filename_template = '{}_{}_group_{:03d}Hz_SNR{:03d}-{}.txt'
    filename_template = os.path.join(outputdir, raw_filename_template)

    # write files out
    t0 = t_start + padding
    for i, (freq, snr) in enumerate(pairs[ifo]):
        with open(filename_template.format(
            ifo, tagname, freq, snr, t0), 'w+') as file:
            for time in pickedup_times[i * 3:i * 3 + 3]:
                if time is not None:
                    file.write(str(time) + '\n')
            t0 += repeats * buffer

    # write "all" file with all injections
    allfile = os.path.join(outputdir, '{}_{}-ALL-{}.txt'.format(
            ifo, tagname, t_start))
    with open(allfile, "w+") as f:
        for time in pickedup_times:
            if time is not None:
                f.write(str(time) + '\n')
    return allfile


def do_asynchronously(func, func_args, func_kwargs={}):
    """Call this function asynchronously as many times as there are args
    in func_args

    Parameters
    ----------
    func: function
        The function to call asynchronously
    func_args: tuple of tuples
        Tuple containing tuples, each containing the arguments for the function
    func_kwargs: dict of kwargs
        but this won't be iterated over
    """
    pool = mp.Pool(mp.cpu_count()/2)
    #processes = [pool.apply_async(func, args=(func_arg),
    #                              kwargs=func_kwargs)
    #             for func_arg in func_args]
    processes = [pool.apply_async(func, args=(func_arg),
                                  kwds=func_kwargs)
                 for func_arg in func_args]
    output = [p.get() for p in processes]


def plot_inj_vs_kw(ifo, t_start, tagname, kw_trgfile_path,
                   outputdir='inj_times',
                   pairs=INJ_PAIRS, padding=INJ_PADDING,
                   buffer=INJ_BUFFER, repeats=INJ_REPEATS):
    """Plot expected injections against kleineWelle triggers.

    Parameters
    ----------
    ifo: str
        'H1' or 'L1'
    t_start: int or float
        Time when the injection waveform begins (ie first injection minus
        buffer)
    tagname: str
        tagname for what to name things for this particular run
    kw_trgfile_path: str
        Path of the *KW_TRIGGERS-ALL.trg file

    Returns
    -------
    str: path to saved plot
    """
    """
    with open(kw_trgfile_path, 'r') as f:
        reader = csv.reader(f, delimiter=' ', skipinitialspace=True)
        kwlist = np.array(reader)
        """
    kwlist = np.genfromtxt(kw_trgfile_path)
    # Calculate KW time, freq
    kw = [kwlist[1:,2], kwlist[1:,3]]

    # Calculate expected injection time, freq, snrs
    inj = [ #time, freq, snr
        [t_start + padding + i*buffer
         for i in range(repeats*len(pairs[ifo]))],
        np.repeat(np.array(pairs[ifo])[:,0], repeats),
        np.repeat(np.array(pairs[ifo])[:,1], repeats)]

    figname = "{}_{}_kw_vs_inj.png".format(ifo, tagname)

    # Plot
    plt.figure(figsize=(12, 8))
    plt.scatter(inj[0], inj[1], c=inj[2], marker='o', label="Injections")
    plt.scatter(kw[0], kw[1], marker='+', label="KleineWelle")
    plt.xlabel("GPS time")
    plt.ylabel("Frequency (Hz)")
    plt.xlim((min(inj[0]) - 10, max(inj[0]) + 10))
    plt.yscale("log")
    plt.legend()
    plt.tight_layout()
    plt.savefig(os.path.join(outputdir, figname), dpi=200)
    plt.close()


def plot_pvalues(names_lpvls, plot_title, filename,
                 cdf=False, density=False, cumulative=True,
                 meaned=False, bins=50, tex=False):
    """names_lpvls is list of (name, lpvl_array) pairs"""
    plt.rcParams['text.usetex'] = tex
    figwidth = 7
    figheight = 5
    dpi = 300

    plt.figure(figsize=(figwidth, figheight))
    returnvals = {}
    for name, lpvls in names_lpvls:
        if cdf:
            the_cdf = make_cdf(lpvls)
            returnval = the_cdf
            plt.plot(*the_cdf, label=name)
        else:
            returnval = plt.hist(
                lpvls, bins=bins, histtype="step", density=density,
                cumulative=cumulative)
        returnvals[name] = returnval

    if cdf:
        plt.ylabel('Fraction of channels')
    else:
        if density:
            plt.ylabel('Density of channels')
        else:
            plt.ylabel('Number of channels')
    plt.yscale('log')
    plt.legend();
    if meaned:
        plt.xlabel('$\mu_{\log{p}}$')
    else:
        plt.xlabel('$\log{p}$')
    plt.title(plot_title)
    plt.grid(True, which='both', linestyle=':')
    plt.savefig(filename, dpi=dpi)
    plt.close()

    return returnvals


def make_cdf(lpvls):
    N = len(lpvls)
    x = np.sort(lpvls)
    f = np.array([i+1 for i in range(N)])/float(N)
    return x, f


def plot_on_and_offsource_cdf(onsource_lpvls_npy, offsource_cdf_npy,
                              plot_title, filename):
    plt.rcParams['text.usetex'] = True
    figwidth = 7
    figheight = 5
    dpi = 500

    offsource_cdf = np.load(offsource_cdf_npy)
    onsource_lpvls = np.load(onsource_lpvls_npy)
    onsource_cdf = make_cdf(onsource_lpvls)

    plt.figure(figsize=(figwidth, figheight))
    plt.plot(*onsource_cdf, label="onsource")
    plt.plot(*offsource_cdf, label="offsource")
    plt.ylabel('Fraction of channels')
    plt.yscale('log')
    plt.xlabel('$\log{p}$')
    plt.legend(loc='upper left')
    plt.title(plot_title)
    plt.grid(True, which='both', linestyle=':')
    plt.savefig(filename, dpi=dpi)
    plt.close()


def strip_ifo_from_chandict(chandict):
    for chan, pval in chandict.items():
        if chan.startswith('H1_') or chan.startswith('L1_'):
            stripped_chan = chan[3:]
            chandict[stripped_chan] = chandict[chan]
            chandict.pop(chan)
    return chandict


def strip_freq_from_chandict(chandict):
    for chan, pval in chandict.items():
        chan_split = chan.split('_')
        if chan_split[-1].isdigit() and chan_split[-2].isdigit():
            stripped_chan = '_'.join(chan_split[:-2])
            pvals = [val for key, val in chandict.items()
                     if stripped_chan in key]
            # if multiple KW chans for each channel, will take the
            # lowest (most significant) pval
            chandict[stripped_chan] = min(pvals)
            chandict.pop(chan)
    return chandict


def compare_two_kw_chanlists(chanlist1, chanlist2, strip_ifo=False,
                             strip_freq=False):
    chandict1 = dict(np.loadtxt(chanlist1, dtype='|S100,float'))
    chandict2 = dict(np.loadtxt(chanlist2, dtype='|S100,float'))

    if strip_ifo:  # remove 'H1_' or 'L1_' identifier
        for chandict in [chandict1, chandict2]:
            chandict = strip_ifo_from_chandict(chandict)

    if strip_freq:  # remove KW frequency
        for chandict in [chandict1, chandict2]:
            chandict = strip_freq_from_chandict(chandict)

    in1notin2_keys = list(set(chandict1.keys()) - set(chandict2.keys()))
    in2notin1_keys = list(set(chandict2.keys()) - set(chandict1.keys()))
    in1notin2_dict = {k:v for k, v in chandict1.items() if k in in1notin2_keys}
    in2notin1_dict = {k:v for k, v in chandict2.items() if k in in2notin1_keys}

    return in1notin2_dict, in2notin1_dict


def combine_two_inj_sets(chanlists1, chanlists2, outdir,
                         strip_ifo=False, strip_freq=False):
    """Takes the pointy channel lists results of two injection sets and
    conservatively combines them into one set of channel lists (ok, warning,
    danger)."""
    chandict_dict = {chanlists1: dict(), chanlists2: dict()}

    for chanlists, chandict in chandict_dict.items():
        for level in ['danger', 'warning', 'ok']:
            chandict.update(np.loadtxt(
                os.path.join(
                    chanlists, 'logpvalue2chans-{}.txt'.format(level)),
                dtype='|S100,float'))

    combined_dict = dict()
    combined_dict.update(chandict_dict.values()[0])
    for chan, pval in chandict_dict.values()[1].items():
        if chan in combined_dict:
            if pval < combined_dict[chan]:
                combined_dict[chan] = pval
        else:
            combined_dict[chan] = pval

    if strip_ifo:
        combined_dict = strip_ifo_from_chandict(combined_dict)
    if strip_freq:
        combined_dict = strip_freq_from_chandict(combined_dict)

    dangerdict = dict()
    warningdict = dict()
    okdict = dict()

    for chan, pval in combined_dict.items():
        if pval > np.log(safety.DEFAULT_PVALUE_WARNING):
            okdict[chan] = pval
        elif np.log(safety.DEFAULT_PVALUE_DANGER) < pval < \
                np.log(safety.DEFAULT_PVALUE_WARNING):
            warningdict[chan] = pval
        else:
            dangerdict[chan] = pval

    dangerlist = list(dangerdict.items())
    warninglist = list(warningdict.items())
    oklist = list(okdict.items())
    for chanlist in [dangerlist, warninglist, oklist]:
        chanlist.sort(key=lambda x: x[1])

    try:
        os.mkdir(outdir)
    except OSError:
        pass

    with open(os.path.join(outdir, 'danger.txt'), "w+") as f:
        for k, v in dangerlist:
            f.write('{} {}\n'.format(k, str(v)))
    with open(os.path.join(outdir, 'warning.txt'), "w+") as f:
        for k, v in warninglist:
            f.write('{} {}\n'.format(k, str(v)))
    with open(os.path.join(outdir, 'ok.txt'), "w+") as f:
        for k, v in oklist:
            f.write('{} {}\n'.format(k, str(v)))

    return dangerlist, warninglist, oklist


def draw_random_valid_times(ifo, wd, t_start, t_end, N, kwtrigpad=1,
                            kw_all_trg=None):
    """Draw N random times between t_start and t_end and make sure that they
    fall in good times (no overflows, in lock)

    Returns
    -------
    np.array: 1d array of N valid times
    """
    from gwpy.timeseries import StateVector
    from gwpy.segments import Segment, SegmentList
    from gwdatafind import find_urls

    # Create GDS-CALIB_STRAIN KW triggers
    if not kw_all_trg:
        kw_frame_txt = write_kw_frames_to_txt(ifo, t_start, t_end, wd,
                                              trailing_frametype='_HOFT_C00')
        calibstrain = 'GDS-CALIB_STRAIN'
        kw_cfg = build_kw_config(ifo, wd, channel=calibstrain)
        calibstrain_trg = run_kw(ifo, wd, t_start, t_end)
    else:
        calibstrain_trg = kw_all_trg
    trigs = np.genfromtxt(calibstrain_trg)
    try:
        trig_t0s = trigs[1:,2]
    except IndexError:  # this list is empty, no trigs found
        trig_t0s = []

    calib_strain_seglist = SegmentList()
    for t0 in trig_t0s:
        calib_strain_seglist.append(Segment(t0 - kwtrigpad, t0 + kwtrigpad))
    calib_strain_seglist.coalesce()
    no_strain_trigs = ~calib_strain_seglist

    # Find times when DMT-DQ_VECTOR and GDS-CALIB_STATE_VECTOR are not good
    # bit definitions
    dmt_dq_bits = ['odd_parity', 'no_omc_dcpd_o/f', 'no_etmy_esd_o/f']
    calib_state_bits = [
        'hoft_ok', 'obs_intent', 'bit2', 'bit3', 'bit4', 'no_stoch_inj',
        'no_cbc_inj', 'no_burst_inj', 'no_detchar_inj']

    # get state vectors
    urls = find_urls(ifo[0], ifo[0] + "1_HOFT_C00", t_start, t_end)
    dmt_dq = StateVector.fetch(ifo + ':DMT-DQ_VECTOR', t_start, t_end,
                               dmt_dq_bits)
    calib_state = StateVector.fetch(ifo + ':GDS-CALIB_STATE_VECTOR',
                                    t_start, t_end, calib_state_bits)
    dmt_dq_flags = dmt_dq.to_dqflags()
    calib_state_flags = calib_state.to_dqflags()

    # combine state vectors with times with no GDS-CALIB_STRAIN trigs
    good_segs =  dmt_dq_flags['no_omc_dcpd_o/f'].active & \
            dmt_dq_flags['no_etmy_esd_o/f'].active & \
            calib_state_flags['hoft_ok'].active & \
            calib_state_flags['no_stoch_inj'].active & \
            calib_state_flags['no_cbc_inj'].active & \
            calib_state_flags['no_burst_inj'].active & \
            calib_state_flags['no_detchar_inj'].active & \
            no_strain_trigs

    times = []
    for n in range(N):
        good_time = False
        while not good_time:
            time = np.random.uniform(t_start, t_end)
            if time in good_segs:
                good_time = True
        times.append(time)

    return np.array(times)


def plot_lpvls_from_N_100(lpvls, outdir):
    # basically a one-time-use function
    lpvls = np.load(lpvls)
    lpvl_list = np.split(lpvls, 100)
    N = float( len(lpvl_list[0]))

    matplotlib.style.use('classic')
    plt.rcParams['text.usetex'] = True
    plt.figure(figsize=(7,5))

    for lpvlset in lpvl_list:
        plt.plot(*make_cdf(lpvlset), c='grey', linewidth=0.5)
    cdf = make_cdf(lpvls)
    sigma = np.sqrt(np.interp(cdf[0], *cdf) / (N * 100))

    plt.plot(*cdf, c='blue', label='normalized sum of all times')
    plt.fill_between(cdf[0], cdf[1] + sigma, cdf[1] - sigma,
                     facecolor='blue', alpha=0.35, lw=0, label='1 sigma')
    plt.legend(loc='upper left')
    plt.grid(True, which='both', linestyle=':')
    plt.yscale('log')
    plt.xlabel('$\log{p}$')
    plt.ylabel('Fraction of channels')
    plt.xlim(right=0.5)
    plt.savefig(os.path.join(outdir,
                             'offsource_cdf_eachtime_and_alltimes.png'),
                dpi=500)
    plt.close()


def hist_given_lpvl(lpvls, outdir, lpvl_to_hist):
    lpvls = np.load(lpvls)
    lpvl_list = np.split(lpvls, 100)
    N = len(lpvl_list[0])

    cdfs = [make_cdf(lpvlset) for lpvlset in lpvl_list]
    counts_at_lpvl = [N * np.interp(lpvl_to_hist, *cdf) for cdf in cdfs]
    mean = np.mean(counts_at_lpvl)
    stdev = np.std(counts_at_lpvl)

    matplotlib.style.use('classic')
    plt.rcParams['text.usetex'] = True
    plt.figure(figsize=(7,5))
    plt.hist(counts_at_lpvl)
    plt.grid(True, which='both', linestyle=':')
    plt.xlabel('Number of channels below lpvl {}'.format(lpvl_to_hist))
    plt.ylabel('Number of times')
    plt.title(
        """histogram of channel counts below lpvl {:.3g}
        mean {:.3g} ({:.3g}\%), stdev {:.3g} ({:.3g}\%)""".format(
            lpvl_to_hist, mean, 100*mean/N, stdev, 100*stdev/N))
    plt.savefig(os.path.join(
        outdir, 'histogram_counts_below_lpvl_{}.png'.format(lpvl_to_hist)),
                dpi=500)
    plt.close()


def plot_cdfs_numtimes(lpvls_split, list_of_randtimes, outdir, average=False):
    """Plots cdfs of lpvls given in lpvls_split using the numbers of times
    given in list_of_randtimes. lpvls_split should be a np.array of np.arrays
    where each inner array has length equal to the number of channels. Each
    inner array also samples the same channels in the same order, so the list
    of each inner array's first entry would be a list of log p-values for the
    same channel at different times.
    """
    from pointy import pointy as ppointy
    lpvls_split = np.load(lpvls_split)
    N = float(len(lpvls_split[0]))

    matplotlib.style.use('classic')
    plt.rcParams['text.usetex'] = True
    plt.figure(figsize=(7,5))
    cycle_colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

    for i, numtimes in enumerate(list_of_randtimes):
        lpvls = []
        if average:
            for i in range(N):
                lpvls.append(ppointy.lognaivebayes(
                    lpvls_split[:numtimes,i]) / (1.*numtimes))
        else:
            lpvls = np.concatenate(lpvls_split[:numtimes])
        cdf = make_cdf(lpvls)
        sigma = np.sqrt(np.interp(cdf[0], *cdf) / (numtimes * N))
        plt.plot(*cdf, linewidth=0.8, label='{} times'.format(
            numtimes))
        plt.fill_between(cdf[0], cdf[1] + sigma, cdf[1] - sigma, alpha=0.35,
                         facecolor=cycle_colors[i], linewidth=0,
                         label='{} times, 1 sigma'.format(numtimes))
    plt.legend(loc='upper left', fontsize='small', ncol=2)
    plt.grid(True, which='both', linestyle=':')
    plt.yscale('log')
    plt.xlabel('$\log{p}$')
    plt.ylabel('Fraction of channels')
    plt.xlim(right=0.5)
    pngname = 'cdfs_for_various_times.png' if not average else \
        'cdfs_for_various_times_averaged.png'
    plt.savefig(os.path.join(outdir, pngname), dpi=500)
    plt.close()


def correlogram_diffs(times1, times2=None, tlimit=None, absval=True,
                      tcluster=0):
    """Takes in a list of times and spits out either the auto-correlation
    or cross-correlation diffs, which can then be plotted.

    Parameters
    ----------
    times1: list of floats
        List of times to autocorrelate or cross-correlate if times2 is given
    times2: list of floats, optional
        List of times to crosscorrelate with times1
    tlimit: int or float
        Maximum time of diffs
    absval: bool, optional
        Whether or not to take the absolute value of the diffs (defaults to
        true)
    """
    def clustering(times, tcluster):
        declustered = []
        times = np.sort(times)
        for i, t in enumerate(times[:-1]):
            if times[i+1] - t > tcluster:
                declustered.append(t)
        return declustered

    times1 = np.sort(times1)
    times2 = np.sort(times2) if times2 is not None else times1

    if tcluster:
        times1 = clustering(times1, tcluster)
        times2 = clustering(times2, tcluster) if times2 is not None else times1

    diffs = []
    for time1 in times1:
        for time2 in times2:
            diff = time1 - time2
            # remove identically zero times
            if diff != 0 and tlimit:
                if abs(diff) < tlimit:
                    diffs.append(diff)
            elif diff != 0:
                diffs.append(diff)
    if absval:
        diffs = np.abs(diffs)
    return diffs


def correlogram_plot(vals_labels, outpath, xlabel, ylabel, title, bins=20,
                     legend=True, histtype='step',
                     xscale='linear', yscale='linear'):
    """Takes either one tuple of (values, label, color, alpha)
    or a list of tuples.
    """
    if type(vals_labels) is not list:
        vals_labels = [vals_labels]
    plt.rcParams['text.usetex'] = True
    matplotlib.style.use('classic')

    for hist_val, label, color, alpha in vals_labels:
        if color is not None and label is not None:
            plt.hist(hist_val, bins=bins, histtype=histtype, label=label,
                 color=color, alpha=alpha)
        elif color is not None:
            plt.hist(hist_val, bins=bins, histtype=histtype, color=color,
                     alpha=alpha)
        elif label is not None:
            plt.hist(hist_val, bins=bins, histtype=histtype, label=label,
                     alpha=alpha)
        else:
            plt.hist(hist_val, bins=bins, histtype=histtype, alpha=alpha)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    if legend:
        plt.legend(loc='best', fontsize=10)
    plt.xscale(xscale)
    plt.yscale(yscale)
    plt.savefig(outpath, dpi=500)
    plt.close()


def lpvls_at_times(lpvl_ts, list_of_times):
    from pointy import utils as putils

    ts_lpvls = []
    array, s, d = putils.load(lpvl_ts, verbose=True)
    lpvl = array['logpvalue']
    for gps in list_of_times:
        this_lpvl = np.interp(gps, np.arange(s, s+d, 1.*d/len(lpvl)), lpvl)
        ts_lpvls.append(this_lpvl)
    return ts_lpvls


def hist_given_lpvl_many_channels_times(wd, chanlist, num_channels,
                                        num_times_per_channel, maxlpvl,
                                        timesnpy, outdir, saveEach=False):
    """Picks a channel from chanlist, draws num_times_per_channel many
    times from timesnpy, picks that number of lpvls from that channel, then
    sees how many of those lpvls are under maxlpvl. Then does this for
    num_channel times, and makes a histogram of the results to hist_out.

    Assumes chanlist is two column space separated as 'channel logpvalue'
    """
    matplotlib.style.use('classic')
    plt.rcParams['text.usetex'] = True
    name = 'histogram_channels_with_times_below_lpvl_{}'.format(maxlpvl)
    if saveEach:
        try:
            os.mkdir(os.path.join(outdir, name))
        except OSError:  # directory exists
            pass
        cdfs = []

    times = np.load(timesnpy)
    loaded_chanlist = np.genfromtxt(chanlist, dtype='|S100,float')
    possible_chans = [pair[0] for pair in loaded_chanlist]

    chosen_chans = np.random.choice(possible_chans, num_channels,
                                    replace=False)

    num_chans_below_maxlpvl = []
    for chan in chosen_chans:
        ts = find_timeseries_for_channel(wd, chan)
        these_lpvls = lpvls_at_times(
            ts, np.random.choice(times, num_times_per_channel, replace=False),
            maxlpvl)
        if saveEach:
            # make and save cdf for each channel
            each_outpath = os.path.join(
                outdir, name, chan + '{}_random_times_cdf.png'.format(
                    num_times_per_channel))

            plt.figure(figsize=(7,5))
            the_cdf = make_cdf(these_lpvls)
            cdfs.append(the_cdf)
            plt.plot(*the_cdf)
            plt.grid(True, which='both', linestyle=':')
            plt.yscale('log')
            plt.xlim(right=0.5)
            plt.ylabel('Fraction of channels')
            plt.xlabel('$\log{p}$')
            plt.title(
                """cdf of {:.3g} random lpvls
                {:}""".format(
                    num_times_per_channel, chan.replace('_', '\_')))
            plt.savefig(each_outpath, dpi=500)
            plt.close()

        lpvls_below_lpvl = [alpvl for alpvl in these_lpvls if alpvl < maxlpvl]
        num_chans_below_maxlpvl.append(len(lpvls_below_lpvl))

    if saveEach:
        # plot all cdfs on one plot
        all_outpath = os.path.join(
            outdir, name,  'all_channels_{}_random_times_cdfs.png'.format(
                num_times_per_channel))

        plt.figure(figsize=(7,5))
        for cdf in cdfs:
            plt.plot(*cdf, linewidth=0.5, alpha=0.8)
        plt.grid(True, which='both', linestyle=':')
        plt.yscale('log')
        plt.xlim(right=0.5)
        plt.ylabel('Fraction of channels')
        plt.xlabel('$\log{p}$')
        plt.title(
            """cdf of {:.3g} random lpvls
            {} channels""".format(
                num_times_per_channel, len(cdfs)))
        plt.savefig(all_outpath, dpi=500)
        plt.close()

    mean = np.mean(num_chans_below_maxlpvl)
    stdev = np.std(num_chans_below_maxlpvl)

    plt.figure(figsize=(7,5))
    plt.hist(num_chans_below_maxlpvl)
    plt.grid(True, which='both', linestyle=':')
    plt.ylabel('Number of channels')
    plt.xlabel('Number of times with lpvl below {}'.format(maxlpvl))
    plt.title(
        """histogram of times with lpvl below lpvl {:.3g}
        mean {:.3g}, stdev {:.3g}, {:.3g} channels, {:.3g} times each""".format(
            maxlpvl, mean, stdev, num_channels, num_times_per_channel))
    plt.savefig(os.path.join(outdir, name + '.png'), dpi=500)
    logging.info(name + " is saved")
    plt.close()


def generate_subsys_sub(
        ifo, wd, subsys, json_settings_path,
        memory='6G', disk='1K', tag='ligo.prod.o4.detchar.explore.test'):
    subsys_path = os.path.join(wd, '{}_{}'.format(ifo, subsys))
    subfile_path = os.path.join(
        subsys_path, '{}_{}.per_subsys.sub'.format(ifo, subsys))
    logs_dir = os.path.join(wd, 'condor_logs')
    logpath = os.path.join(logs_dir, 'per_subsys.log')
    outpath = os.path.join(logs_dir, '{}_{}.per_subsys.out'.format(ifo, subsys))
    errpath = os.path.join(logs_dir, '{}_{}.per_subsys.err'.format(ifo, subsys))
    args = '{} {}'.format(json_settings_path, subsys)

    contents = dedent('''\
    Universe   = vanilla
    Executable = /home/geoffrey.mo/hwinj/streamline/safety/pipeline_condor/per_subsys.sh
    Arguments  = {args}
    Log        = {logpath}
    Output     = {outpath}
    Error      = {errpath}
    accounting_group = {tag}
    request_memory = {memory}
    request_disk = {disk}
    should_transfer_files = YES
    getenv = True
    Queue''').format(
        args=args, logpath=logpath, outpath=outpath,
        errpath=errpath, tag=tag, memory=memory, disk=disk)

    with open(subfile_path, 'w+') as f:
        f.write(contents)
    logging.info('subsystem subfile for {}_{} written to {}'.format(
        ifo, subsys, subfile_path))

    return subfile_path


def generate_postprocess_sub(
        wd, json_settings_path,
        memory='4G', disk='1K', tag='ligo.prod.o4.detchar.explore.test'):
    subfile_path = os.path.join(wd, 'postprocess.sub')
    logs_dir = os.path.join(wd, 'condor_logs')
    logpath = os.path.join(logs_dir, 'postprocess.log')
    outpath = os.path.join(logs_dir, 'postprocess.out')
    errpath = os.path.join(logs_dir, 'postprocess.err')
    args = json_settings_path

    contents = dedent('''\
    Universe   = vanilla
    Executable = /home/geoffrey.mo/hwinj/streamline/safety/pipeline_condor/postprocess.sh
    Arguments  = {args}
    Log        = {logpath}
    Output     = {outpath}
    Error      = {errpath}
    accounting_group = {tag}
    request_memory = {memory}
    request_disk = {disk}
    getenv = True
    Queue''').format(
        args=args, logpath=logpath, outpath=outpath,
        errpath=errpath, tag=tag, memory=memory, disk=disk)

    with open(subfile_path, 'w+') as f:
        f.write(contents)
    logging.info('postprocess subfile for written to {}'.format(
        subfile_path))

    return subfile_path


def generate_dag(
        ifo, wd, tagname, subsys_sub, postprocess_sub_path):
    # subsys_sub is a list of [[subsys, path_to_subsys_sub]]
    dag_path = os.path.join(wd, '{}_{}.dag'.format(ifo, tagname))

    subsys_jobs = ''
    for subsys, sub in subsys_sub:
        subsys_jobs += 'Job {subsys} {sub}\n'.format(
            subsys=subsys, sub=sub
        )
    subsyses = [subsys for subsys, sub in subsys_sub]
    subsyses_str = ' '.join(subsyses)

    contents = '''{}Job Postprocess {}
PARENT {} CHILD Postprocess'''.format(subsys_jobs, postprocess_sub_path, subsyses_str)

    with open(dag_path, 'w+') as f:
        f.write(contents)
    logging.info('dag written to {}'.format(
        dag_path))

    return dag_path


def get_offsource_kw_start_end(ifo, wd, tagname):
    file = glob.glob(os.path.join(
        wd, '{}-KW_{}_R_RDS{}_TRIGGERS_rds_chanlist*.txt'.format(
        ifo[0], ifo, tagname
        )))[0]
    file_nameonly = os.path.basename(file).split('.txt')[0]
    dashsplit = file_nameonly.split('-')
    start, dur = int(dashsplit[-2]), int(dashsplit[-1])
    return start, start + dur


def split_chan_pvals_danger_warning(chan_pvals, danger_pval, warning_pval):
    '''Splits list of channels and pvalues into ok, warning, and danger.

    Parameters
    ----------
    chan_pvals : list of (str, float)
        List of (channel, pvalue) pairs
    danger_pval : float
        Danger p-value
    warning_pval : float
        Warning p-value
    '''
    resorted = sorted(chan_pvals, key=lambda x: x[1])
    pvals = np.array([pval for chan, pval in resorted])
    danger_ind = np.where(pvals < danger_pval)[0].max()
    warning_ind = np.where(pvals < warning_pval)[0].max()

    danger = resorted[:danger_ind+1]
    warning = resorted[danger_ind+1 : warning_ind+1]
    ok = resorted[warning_ind+1:]

    return ok, warning, danger


# def plot_lpvl_dists(names_npys, outdir, filename):
#     """List of (name, path_to_npy) pairs"""
#     outpath = os.path.join(outdir, filename)
#     data = utils.plot_pvalues(
#         lpvls,
#         "Log p-values from {} random offsource times\n{} {}".format(
#             N, ifo, tagname),
#         os.path.join(offsource_dir, filename),
#         cdf=cdf, cumulative=cumulative, tex=tex)
#     dataname = "cdf.npy" if cdf else "hist.npy"
#     np.save(os.path.join(offsource_dir, dataname), data)