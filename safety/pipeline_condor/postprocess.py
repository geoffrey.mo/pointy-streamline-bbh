import argparse
import logging
import os
import sys
import numpy as np
import json
sys.path.append('/home/geoffrey.mo/hwinj/streamline/safety/')
import safety, utils, omegascanner

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=os.environ.get("LOGLEVEL", "INFO"))

parser = argparse.ArgumentParser()
parser.add_argument('jsonpath', type=str, help='path to json settings file')
args = parser.parse_args()

with open(args.jsonpath, 'r') as openfile:
    argdict = json.load(openfile)

ifo = argdict['ifo']
wd = argdict['wd']
tagname = argdict['tagname']
danger_fap = argdict['danger_fap']
warning_fap = argdict['warning_fap']
detchar_channel_list = argdict['detchar_channel_list']
kw_pickedup_times = argdict['kw_pickedup_times']
t_start = float(argdict['t_start'])

logging.info('Making one big list of channels and pvalues')
chan_pvals = safety.stacked_to_onelist(ifo, wd, tagname)
logging.info('Getting offsource times')
t_offsource_start, t_offsource_end = utils.get_offsource_kw_start_end(ifo, wd, tagname)
# stacked stacks the different times, and normalize takes the avg pval of those times
# only_good_times checks state vectors first for hoft_ok and DMT overflows,
# but this is statisically unlikely so unnecessary most of the time
offsource_lpvls = safety.offsource_distribution(
    ifo, wd, tagname, t_offsource_start, t_offsource_end, N=100,
    normalize=True, stack=True, cdf=True, cumulative=True,
    save_times=True, only_good_times=False,
    )
danger_pval = np.percentile(offsource_lpvls, danger_fap)
warning_pval = np.percentile(offsource_lpvls, warning_fap)

logging.info('Splitting up KW channels into ok, danger, and warning')
ok, warning, danger = utils.split_chan_pvals_danger_warning(chan_pvals, danger_pval, warning_pval)
for name, lists in zip(['ok', 'warning', 'danger'], [ok, warning, danger]):
    path = os.path.join(wd, 'chanlists_{}'.format(tagname), 'kwchans_pvals_{}.txt'.format(name))
    logging.info('Writing {} KW list to {}'.format(name, path))
    with open(path, 'w+') as f:
        for chan, pval in lists:
            f.write(chan + ' ' + str(pval) + '\n')
onsource_lpvls = np.array([pval for chan, pval in chan_pvals])

logging.info('Making a plot of the offsource vs onsource pvalues')
utils.plot_pvalues(
    [('offsource', offsource_lpvls), ('onsource', onsource_lpvls)],
    'Log p-values from {} {}'.format(ifo, tagname),
    os.path.join(wd, 'chanlists_{}'.format(tagname), 'lpvl_distrs.png'),
    cdf=True, meaned=True, cumulative=True)

logging.info('Making lists of frequency-combined channels')
kwchandict = {chan: pval for chan, pval in chan_pvals}
channeldict = utils.strip_freq_from_chandict(kwchandict)
channel_pvals = [(channel[:2] + ':' + channel[3:], pval) for channel, pval in channeldict.items()]

logging.info('Splitting up frequency-combined channels into ok, danger, and warning')
ok, warning, danger = utils.split_chan_pvals_danger_warning(channel_pvals, danger_pval, warning_pval)
for name, lists in zip(['ok', 'warning', 'danger'], [ok, warning, danger]):
    path = os.path.join(wd, 'chanlists_{}'.format(tagname), 'channels_pvals_{}.txt'.format(name))
    logging.info('Writing {} channel list to {}'.format(name, path))
    with open(path, 'w+') as f:
        for chan, pval in lists:
            f.write(chan + ' ' + str(pval) + '\n')
path = os.path.join(wd, 'chanlists_{}'.format(tagname), 'channels_pvals_all.txt')
logging.info('Writing all channel list to {}'.format(path))
all_chan_pval = sorted(channel_pvals, key=lambda x: x[1])
with open(path, 'w+') as f:
    for chan, pval in all_chan_pval:
        f.write(chan + ' ' + str(pval) + '\n')

pointy_chanlist_dir = os.path.join(wd, 'chanlists_{}'.format(tagname))
logging.info('Comparing pointy output in {} to detchar channel list ({})'.format(
    pointy_chanlist_dir, detchar_channel_list))
safety.compare_channel_list(
    detchar_channel_list,
    os.path.join(pointy_chanlist_dir, 'channels_pvals_ok.txt'),
    os.path.join(pointy_chanlist_dir, 'channels_pvals_all.txt'),
    outname_full_list=os.path.join(
        pointy_chanlist_dir, 'chanlists_compared_to_ini.csv')
)

logging.info('Making omegascans for top 500 unsafe channels')
omegascan_dir = os.path.join(wd, 'omegascans')
channels_to_scan = [chan for chan, pval in all_chan_pval[:500]]
try:
    os.makedirs(omegascan_dir)
except OSError:  # directory already exists
    pass
omegascanner.main(ifo, t_start, 440, omegascan_dir, channels_to_scan,
                 inj_time_txt=kw_pickedup_times)

logging.info('Done!')
