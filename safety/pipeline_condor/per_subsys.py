import argparse
import json
import logging
import os
import sys
sys.path.append('/home/geoffrey.mo/hwinj/streamline/safety/')
import safety

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=os.environ.get("LOGLEVEL", "INFO"))

parser = argparse.ArgumentParser()
parser.add_argument('jsonpath', type=str, help='path to json settings file')
parser.add_argument('subsys', type=str, help='name of this subsys')
args = parser.parse_args()

with open(args.jsonpath, 'r') as openfile:
    argdict = json.load(openfile)

safety.make_pval_timeseries_to_stacked_for_subsys(
    argdict['ifo'], argdict['wd'], argdict['tagname'],
    args.subsys, argdict['gps_groups']
)