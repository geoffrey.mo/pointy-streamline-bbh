import argparse
import json
import os
import sys
import logging
import numpy as np

sys.path.append('/home/geoffrey.mo/hwinj/streamline/safety/')
import safety
import utils

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    level=os.environ.get("LOGLEVEL", "INFO"))

parser = argparse.ArgumentParser()
parser.add_argument('ifo', type=str, help='ifo to analyze ("H1" or "L1")')
parser.add_argument('wd', type=str, help='working directory for this run')
parser.add_argument('tagname', type=str, help='tagname for this run')
parser.add_argument('t_start', help='start gpstime of injection waveform')
parser.add_argument('--danger_fap', type=float, default=0.01, help='false alarm probability for danger channels')
parser.add_argument('--warning_fap', type=float, default=0.1, help='false alarm probability for warning channels')
parser.add_argument('--detchar_channel_list', type=str,
                    default='/home/geoffrey.mo/ligo-channel-lists/O4/XX-O4-standard.ini',
                    help='path to ligo-channel-list ini file with XX instead of H1 or L1')


args = parser.parse_args()
ifo = args.ifo
wd = os.path.abspath(args.wd)
tagname = args.tagname
t_start = float(args.t_start)
detchar_channel_list_path = args.detchar_channel_list.replace('XX', ifo)

try:
    os.makedirs(wd)
except OSError:  # directory already exists
    pass
# sys.stdout = open(os.path.join(wd, "{}_{}.out".format(ifo, tagname)), "w+")
# sys.stderr = open(os.path.join(wd, "{}_{}.err".format(ifo, tagname)), "w+")

# make triggers for all injections
logging.info('Making file with all injections')
kw_all = utils.inj_t1900555_to_txt(ifo, t_start, tagname, outputdir=wd)
kwtrgfile = os.path.join(
    wd, "{}-KW_TRIGGERS-{}/{}-KW_TRIGGERS-ALL.trg".format(
        ifo[0], str(t_start)[:5], ifo[0]))
logging.info('Making plot of injections versus recovered KW triggers')
utils.plot_inj_vs_kw(ifo, t_start, tagname, kwtrgfile, outputdir=wd)

gps_groups = [('ALL', kw_all)]
logging.info('GPS groups are {}'.format(gps_groups))

logging.info('Making big RDS channel list')
rdschan_path = safety.make_big_rds_channellist(ifo, wd, tagname, gps_groups)
logging.info('Splitting into subsystems')
subsys_list = safety.split_into_subsys(ifo, wd, rdschan_path)

jsonpath = os.path.join(wd, 'post_subsys_split_settings.json')
logging.info('Writing json settings to {}'.format(jsonpath))
with open(jsonpath, "w") as outfile:
    json.dump({
        'ifo': ifo,
        'wd': wd,
        'tagname': tagname,
        'subsys_list': subsys_list,
        'gps_groups': gps_groups,
        'danger_fap': args.danger_fap,
        'warning_fap': args.warning_fap,
        'detchar_channel_list': detchar_channel_list_path,
        'kw_pickedup_times': kw_all,
        't_start': t_start,
    },
    outfile, indent=4)

condor_logsdir = os.path.join(os.path.join(wd, 'condor_logs'))
logging.info('Making condor subdirectory: {}'.format(condor_logsdir))
try:
    os.makedirs(condor_logsdir)
except OSError:  # directory already exists
    pass

logging.info('Generating submit files for subsystems')
subsys_sub = []
for subsys in subsys_list:
    sub_path = utils.generate_subsys_sub(
        ifo, wd, subsys, jsonpath,
        memory='4G', disk='2G', tag='ligo.prod.o4.detchar.explore.test')
    subsys_sub.append(['{}_{}'.format(ifo, subsys), sub_path])

logging.info('Generating submit file for postprocessing')
postprocess_sub_path = utils.generate_postprocess_sub(
    wd, jsonpath)

logging.info('Generating dag')
dag_path = utils.generate_dag(ifo, wd, tagname, subsys_sub, postprocess_sub_path)

logging.info('Submitting dag')
os.system('cd {} && condor_submit_dag -import_env -f {}'.format(wd, dag_path))
