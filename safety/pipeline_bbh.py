import argparse
import os
import sys

import safety
import utils

parser = argparse.ArgumentParser()
parser.add_argument('ifo', type=str, help='ifo to analyze ("H1" or "L1")')
parser.add_argument('wd', type=str, help='working directory for this run')
parser.add_argument('tagname', type=str, help='tagname for this run')
parser.add_argument('t0', help='BBH t0')
parser.add_argument('--frac_dur', type=float, default=0.01)
parser.add_argument('--coinc_window', type=float, default=1.0)

args = parser.parse_args()
ifo = args.ifo
wd = os.path.abspath(args.wd)
tagname = args.tagname
t0 = float(args.t0)

sys.stdout = open(os.path.join(wd, "{}_{}.out".format(ifo, tagname)), "w+")
sys.stderr = open(os.path.join(wd, "{}_{}.err".format(ifo, tagname)), "w+")

t0_file = os.path.join(wd, "{}_t0.txt".format(tagname))
with open(t0_file, "w+") as f:
    f.write(str(t0) + '\n')
gps_groups = [(tagname, t0_file)]

rdschan_path = safety.make_big_rds_channellist(
    ifo, wd, tagname, gps_groups, frac_dur=args.frac_dur,
    pp_window=args.coinc_window)
subsys_list = safety.split_into_subsys(ifo, wd, rdschan_path)

async_args_tuple = tuple([
    tuple([ifo, wd, tagname, subsys, gps_groups]) for subsys in subsys_list])
utils.do_asynchronously(
    safety.make_pval_timeseries_to_stacked_for_subsys, async_args_tuple,
    func_kwargs={'frac_dur': args.frac_dur,
                 'pp_window': args.coinc_window})

safety.stacked_to_chanlists(ifo, wd, tagname)
